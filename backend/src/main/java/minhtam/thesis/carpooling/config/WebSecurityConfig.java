
package minhtam.thesis.carpooling.config;

import org.apache.coyote.http2.Http2Protocol;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/authenticate", "/register").antMatchers("/v2/api-docs")
				.antMatchers("/swagger-resources/**").antMatchers("/swagger-ui.html").antMatchers("/resources/**")
				.antMatchers("/static/**").antMatchers("/henhomimage/**").antMatchers("/pdffile/**").antMatchers("/matadoorstatic/**").antMatchers("/formmuanhom/**").antMatchers("/formbaogia/**").antMatchers("/formlapghep/**").antMatchers("/mauimage/**")
				.antMatchers("/configuration/**").antMatchers("/webjars/**").antMatchers("/public")
				.antMatchers("/public/**").antMatchers("/_data/**").antMatchers("/_data").antMatchers("/user/**")

				.and().ignoring().antMatchers("/h2-console/**/**");
		;
	}

	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.cors();
		httpSecurity.csrf().disable().authorizeRequests().antMatchers("/authenticate").permitAll().antMatchers("/verifyIdToken").permitAll()
		.antMatchers("/firebase-login-email").permitAll().antMatchers("/findByEmailAsync/**").permitAll().antMatchers("/findAll").permitAll()
				
				.antMatchers("/register").permitAll().antMatchers("/driver/**").hasAuthority("DRIVER")
				.antMatchers("/passenger/**").hasAuthority("PASSENGER")
				.antMatchers("/user/**").authenticated().

				anyRequest().permitAll().and().exceptionHandling()
				.authenticationEntryPoint(jwtAuthenticationEntryPoint);

		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		;
	}

	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
	

}
