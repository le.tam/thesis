package minhtam.thesis.carpooling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minhtam.thesis.carpooling.model.RoleDao;

@Repository
public interface RoleRepository extends JpaRepository<RoleDao, Integer> {
	RoleDao findByRole(String role);

}