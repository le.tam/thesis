package minhtam.thesis.carpooling.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import minhtam.thesis.carpooling.model.UserDao;

@Repository
public interface UserRepository extends CrudRepository<UserDao, Integer> {
    UserDao findByUsername(String username);
    
    UserDao findByEmail(String email);
    
    List<UserDao> findAll(Pageable pageable);
    

    
    @Query(value = "select * from users u  where u.id = ?1", nativeQuery = true)
    UserDao findByID(long id);
    
    @Query(value = "select * from user_role ur  where ur.user_id = ?1", nativeQuery = true)
    UserDao findByIDInUserRole(long id);
        
    List<UserDao> findByGroupid(long groupid);
    
    List<UserDao> findByStatus(String status);

//  #List<UserDao> findByRole_String(String role_string);
    
    
    @Query(value = "select * from users u  where u.role_string = ?1", nativeQuery = true)
    List<UserDao> findByRole_String(String role_string);

    

    
}