package minhtam.thesis.carpooling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minhtam.thesis.carpooling.model.Coordinate;



@Repository
public interface CoordinateRepository extends JpaRepository<Coordinate, Long>{
	List<Coordinate> findByUserid(long userid);
	Coordinate findByUseridAndType(long userid,String type);	

}
