package minhtam.thesis.carpooling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minhtam.thesis.carpooling.model.Coordinate;
import minhtam.thesis.carpooling.model.Trips;
import minhtam.thesis.carpooling.model.Vehicles;

@Repository
public interface TripRepository extends JpaRepository<Trips, Long>{
	Trips findByGroupid(long groupid);	

}

