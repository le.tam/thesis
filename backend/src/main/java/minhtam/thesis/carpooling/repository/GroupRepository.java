package minhtam.thesis.carpooling.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minhtam.thesis.carpooling.model.Group;



@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
	
	Group findByUserid(long userid);	
	
}