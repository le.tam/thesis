package minhtam.thesis.carpooling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minhtam.thesis.carpooling.model.Vehicles;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicles, Long> {
	Vehicles findByUserid(long userid);

	void deleteByUserid(long userid);	

}
