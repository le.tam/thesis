package minhtam.thesis.carpooling.controller;

//import com.google.firebase.auth.*;
import minhtam.thesis.carpooling.config.JwtTokenUtil;
import minhtam.thesis.carpooling.model.*;
import minhtam.thesis.carpooling.service.CoordinateService;
import minhtam.thesis.carpooling.service.GroupService;
import minhtam.thesis.carpooling.service.JwtUserDetailsService;
import minhtam.thesis.carpooling.service.VehicleService;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.FilenameUtils;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.api.client.http.HttpHeaders;
import com.google.firebase.auth.ExportedUserRecord;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.ListUsersPage;
import com.google.firebase.auth.UserRecord;

import io.jsonwebtoken.ExpiredJwtException;

import io.swagger.annotations.*;
import minhtam.thesis.carpooling.model.*;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private GroupService groupService;

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private CoordinateService coordinateService;

//	@RequestMapping(value = "/admin/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Returns user from userID", response = List.class)
//	@ApiResponses(value = { 
//			@ApiResponse(code = 400, message = "Something went wrong"), 
//			@ApiResponse(code = 403, message = "Access denied"), 
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getUserById(@ApiParam("id") @PathVariable long id) throws Exception {
//		UserDao user = userDetailsService.findByID(id);
//		if (user == null)
//			return ResponseEntity.badRequest().build();
//		JSONObject result = new JSONObject();
//		JSONArray fgh = new JSONArray();
//		JSONObject sampleInnerElement = new JSONObject();
//		sampleInnerElement.put("doc_url", user.getDoc_url());
//		sampleInnerElement.put("youtube_url", user.getYoutube_url());
//		sampleInnerElement.put("admin_support", user.getAdmin_support());
//		sampleInnerElement.put("hotline", user.getHotline());
//		
//		result.put("config", sampleInnerElement);
//		result.put("id", user.getId());
//		result.put("uid", user.getUid());
//		result.put("username", user.getName());
//		result.put("displayname", user.getDisplayname());
//		result.put("email", user.getEmail());
//		result.put("phone", user.getPhone());
//		result.put("facebook", user.getFacebook());
//		result.put("address", user.getAddress());
//		result.put("loginMethod", user.getLoginMethod());
//		result.put("photourl", user.getPhotourl());
//		result.put("active", user.getActive());
//		result.put("registerDate", user.getRegisterDate());
//		result.put("loginTime", user.getLoginTime());
////		userdao.setName(obj.getString("username"));
////		userdao.setDisplayname(obj.getString("displayname"));
////		userdao.setEmail(obj.getString("email"));
////		userdao.setPhone(obj.getString("phone"));
////		userdao.setFacebook(obj.getString("facebook"));
////		userdao.setAddress(obj.getString("address"));
////		userdao.setLoginMethod(obj.getString("loginMethod"));
////		userdao.setPhotourl(obj.getString("photourl"));
////		userdao.setActive(obj.getBoolean("active"));
////		userdao.setRegisterDate(obj.getLong("registerDate"));
////		userdao.setLoginTime(obj.getLong("loginTime"));
//		return new ResponseEntity<>(result.toMap(), HttpStatus.OK);
//		//return ResponseEntity.ok(userDetailsService.findByID(id));
//		
//	}

	
	
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Returns user from userID", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity getUserById(@ApiParam("id") @PathVariable long id) throws Exception {
		UserDao user = userDetailsService.findByID(id);
		if (user == null)
			return ResponseEntity.badRequest().body("Non-exist user with user id");
		return ResponseEntity.ok(user);

	}

//	@RequestMapping(value = "/user/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Search nearest driver or passenger")
//	@ApiResponses(value = { 
//			@ApiResponse(code = 400, message = "Something went wrong"), 
//			@ApiResponse(code = 403, message = "Access denied"), 
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getUserById() throws Exception {
//		
////		okhttp3.RequestBody requestBody = new MultipartBody.Builder()
////			      .setType(MultipartBody.FORM)
////			      .addFormDataPart("username", "test")
////			      .addFormDataPart("password", "test")
////			      .addFormDataPart("file", "file.txt",
////			    		  okhttp3.RequestBody.create(okhttp3.MediaType.parse("application/octet-stream"), 
////			          new File("src/test/resources/test.txt")))
////			      .build();
//		
//		
//		
//		
////        OkHttpClient client = new OkHttpClient();
////        okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json");
////        okhttp3.RequestBody body = okhttp3.RequestBody.create( "{\n  \"vehicles\": [\n    {\n      \"vehicle_id\": \"my_vehicle\",\n      \"start_address\": {\n        \"location_id\": \"berlin\",\n        \"lon\": 13.406,\n        \"lat\": 52.537\n      }\n    }\n  ],\n  \"services\": [\n    {\n      \"id\": \"hamburg\",\n      \"name\": \"visit_hamburg\",\n      \"address\": {\n        \"location_id\": \"hamburg\",\n        \"lon\": 9.999,\n        \"lat\": 53.552\n      }\n    },\n    { \n     \"id\": \"munich\",\n      \"name\": \"visit_munich\",\n      \"address\": {\n        \"location_id\": \"munich\",\n        \"lon\": 11.57,\n        \"lat\": 48.145\n      }\n    }\n  ]}",mediaType);
////        Request request = new Request.Builder()
////                .url("https://graphhopper.com/api/1/vrp?key=api_key")
////                .post(body)
////                .addHeader("content-type", "application/json")
////                .build();
////
////        Response response = client.newCall(request).execute();
////		System.out.println(response.body().toString());
////		System.out.println(response.toString());
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String username = "";
//		if (principal instanceof UserDetails)
//			username = ((UserDetails) principal).getUsername();
//		else {
//			UserDao principalUserDao = (UserDao)principal;
//			username = principalUserDao.getUsername();
//		}
//		long userid = userDetailsService.findByUsername(username).getId();
//
//		UserDao passenger = userDetailsService.findByID(userid);
//		
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); 
//		LocalDateTime passengerE = LocalDateTime.parse(passenger.getE_time_start(), formatter);
//		
//		
//		LocalDateTime passengerL = LocalDateTime.parse(passenger.getL_time_start(), formatter);
//		
//		
//
//		Coordinate pasorigin = coordinateService.findByUseridAndType(userid, "origin");
//		Coordinate pasdes = coordinateService.findByUseridAndType(userid, "destination");
//
//		
//		URIBuilder ub = new URIBuilder("https://graphhopper.com/api/1/vrp");
//		ub.addParameter("key", "741a409f-3ca2-4565-a183-0a519ac289e3");
//		RestTemplate restTemplate = new RestTemplate();
//		//JSONObject jsonObject = new JSONObject("{\n  \"vehicles\": [\n    {\n      \"vehicle_id\": \"my_vehicle\",\n      \"start_address\": {\n        \"location_id\": \"berlin\",\n        \"lon\": 13.406,\n        \"lat\": 52.537\n      }\n    }\n  ],\n  \"services\": [\n    {\n      \"id\": \"hamburg\",\n      \"name\": \"visit_hamburg\",\n      \"address\": {\n        \"location_id\": \"hamburg\",\n        \"lon\": 9.999,\n        \"lat\": 53.552\n      }\n    },\n    { \n     \"id\": \"munich\",\n      \"name\": \"visit_munich\",\n      \"address\": {\n        \"location_id\": \"munich\",\n        \"lon\": 11.57,\n        \"lat\": 48.145\n      }\n    }\n  ]}");
//		//System.out.println(jsonObject);
//		
//		List<UserDao> listPossibleDriver = new ArrayList<UserDao>();
//		JSONObject jsonObjResult = new JSONObject();
//		JSONArray jsonVehicleResult = new JSONArray();
//		JSONArray jsonServiceResult = new JSONArray();
//		for(Group group : groupService.findAll(null, null, "id", Direction.DESC))
//		{
//			UserDao driver = userDetailsService.findByID(group.getUserid());
//			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); 
//			LocalDateTime driverE = LocalDateTime.parse(driver.getE_time_start(), formatter);
//			LocalDateTime driverL = LocalDateTime.parse(driver.getL_time_start(), formatter);
//			if(group.getStatus() != "full" && (userDetailsService.findByGroupId(group.getId()).size() < vehicleService.findByUserid(driver.getId()).getSeat()))
//			{	//1-((p ets < d ets & p lts < d ets) or (p ets > d lts & p lts > d lts))
//				if(((passengerE.isBefore(driverE) && passengerL.isBefore(driverE))
//						||  (passengerE.isAfter(driverL) && passengerL.isAfter(driverE)) ) == false)
//				{
//				listPossibleDriver.add(driver);
//				}
//				
//			}
//		}
//		JSONObject item = new JSONObject();
//		item.put("vehicle_id", passenger.getUsername());
//		JSONObject startadd = new JSONObject();
//		
//		startadd.put("location_id", pasorigin.getName());
//		startadd.put("lon", pasorigin.getLongitude());
//		startadd.put("lat", pasorigin.getLatitude());
//		item.put("start_address", startadd);
//		
//		jsonVehicleResult.put(item);
//		jsonObjResult.put("vehicles", jsonVehicleResult);
//		for(UserDao possibleDrive: listPossibleDriver)
//		{
//			JSONObject add = new JSONObject();
//			JSONObject cordriver = new JSONObject();
//			cordriver.put("id", possibleDrive.getId());
//			cordriver.put("name", possibleDrive.getUsername());
//			
//			add.put("location_id", pasorigin.getName());
//			add.put("lon", pasorigin.getLongitude());
//			add.put("lat", pasorigin.getLatitude());
//			cordriver.put("address", add);
//			jsonServiceResult.put(cordriver);
//		}
//		jsonObjResult.put("services", jsonServiceResult);
//		
//		ResponseEntity<Map> response = restTemplate.postForEntity(ub.toString(), jsonObjResult.toMap(),
//				Map.class);
//		Map<String, String> result = response.getBody();
//		
//		
//		return ResponseEntity.ok().body(result);
//		
//	}

	@RequestMapping(value = "/user/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Search nearest driver or passenger")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity getUserById() throws Exception {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("https://graphhopper.com/api/1/matrix")
				.queryParam("key", "fb8bc439-42ea-496c-b9e7-b1df004e9c3e")
//		        .queryParam("point", "49.932707,11.588051")
//		        .queryParam("point", "50.241935,10.747375")
//		        .queryParam("point", "50.118817,11.983337")
				.queryParam("type", "json").queryParam("vehicle", "car").queryParam("debug", true)
				.queryParam("out_array", "distances");
		// HttpEntity<?> entity = new HttpEntity<>(headers);
		RestTemplate restTemplate = new RestTemplate();

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao) principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();

		try {
			UserDao passenger = userDetailsService.findByID(userid);

			//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			//LocalDateTime passengerE = LocalDateTime.parse(passenger.getE_time_start(), formatter);
			LocalTime passengerE = LocalTime.parse(passenger.getE_time_start());
			LocalTime passengerL = LocalTime.parse(passenger.getL_time_start());

			//LocalDateTime passengerL = LocalDateTime.parse(passenger.getL_time_start(), formatter);

			URIBuilder ub = new URIBuilder("https://graphhopper.com/api/1/vrp");
			ub.addParameter("key", "fb8bc439-42ea-496c-b9e7-b1df004e9c3e");
			
			
			// JSONObject jsonObject = new JSONObject("{\n \"vehicles\": [\n {\n
			// \"vehicle_id\": \"my_vehicle\",\n \"start_address\": {\n \"location_id\":
			// \"berlin\",\n \"lon\": 13.406,\n \"lat\": 52.537\n }\n }\n ],\n \"services\":
			// [\n {\n \"id\": \"hamburg\",\n \"name\": \"visit_hamburg\",\n \"address\":
			// {\n \"location_id\": \"hamburg\",\n \"lon\": 9.999,\n \"lat\": 53.552\n }\n
			// },\n { \n \"id\": \"munich\",\n \"name\": \"visit_munich\",\n \"address\":
			// {\n \"location_id\": \"munich\",\n \"lon\": 11.57,\n \"lat\": 48.145\n }\n
			// }\n ]}");
			// System.out.println(jsonObject);

			// List<UserDao> listPossibleDriver = new ArrayList<UserDao>();

			JSONObject jsonObjResult = new JSONObject();

			JSONArray jsonVehicleResult = new JSONArray();
			JSONArray jsonServiceResult = new JSONArray();
			Coordinate dStartCoor = coordinateService.findByUseridAndType(userid, "origin");
			Coordinate dDesCoor = coordinateService.findByUseridAndType(userid, "destination");
			if(dStartCoor == null || dDesCoor == null)
			{
				return ResponseEntity.badRequest().body("Missing passenger coordinate");

			}
			builder.queryParam("point", dStartCoor.getLongitude() + "," + dStartCoor.getLatitude());
			List<UserDao> listRequestUser = new ArrayList<>();
			listRequestUser.add(passenger);

			for (Group group : groupService.findAll(null, null, "id", Direction.DESC)) {
				UserDao driver = userDetailsService.findByID(group.getUserid());
				System.out.println(driver.getId());

				if(coordinateService.findByUseridAndType(driver.getId(), "origin")!=null && coordinateService.findByUseridAndType(driver.getId(), "destination")!=null && vehicleService.findByUserid(userid) != null)
				{
					if(!driver.getE_time_start().isEmpty() && driver.getE_time_start()!=null && !driver.getL_time_start().isEmpty() && driver.getL_time_start()!=null) {  
				//formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//				LocalDateTime driverE = LocalDateTime.parse(driver.getE_time_start(), formatter);
//				LocalDateTime driverL = LocalDateTime.parse(driver.getL_time_start(), formatter);
//				
				LocalTime driverE = LocalTime.parse(passenger.getE_time_start());
				LocalTime driverL = LocalTime.parse(passenger.getL_time_start());
				
				if (((passengerE.isBefore(driverE) && passengerL.isBefore(driverE))
						|| (passengerE.isAfter(driverL) && passengerL.isAfter(driverE))) == false) {
					if (group.getStatus() != "full" && (userDetailsService.findByGroupId(group.getId())
							.size() < vehicleService.findByUserid(driver.getId()).getSeat())) { // 1-((p ets < d ets & p
																								// lts < d ets) or (p
																								// ets > d lts & p lts >
																								// d lts))

						// listPossibleDriver.add(driver);
						dStartCoor = coordinateService.findByUseridAndType(driver.getId(), "origin");
						builder.queryParam("point", dStartCoor.getLongitude() + "," + dStartCoor.getLatitude());
						listRequestUser.add(driver);

					}

				}
					}
			}
			}
			HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
					// entity,
					null, String.class);

			try {
				String stringObject = new JSONObject(response).getString("body"); // .getJSONObject("body");
				JSONObject jsonObject = new JSONObject(stringObject);
				JSONArray distance = jsonObject.getJSONArray("distances").getJSONArray(0);// getJSONObject("distances");

				System.out.println(distance);
				if(distance.length() == 1 && distance.getDouble(0) == 0)
				{
					return ResponseEntity.ok(groupService.findAll(0, 5, "id", Direction.ASC));

				}
				double minDistance = 0.0;
				int idxMinDistance = 0;
				for (int i = 0; i < distance.length(); i++) {

					System.out.println(distance.getDouble(i));
					if (minDistance > distance.getDouble(i)) {
						minDistance = distance.getDouble(i);
						idxMinDistance = i;
					}
				}

			}

			catch (Exception e) {
				e.printStackTrace();
			}
			// JSONArray distance = jsonObject.getJSONObject("body");
			// .getJSONArray("distances").getJSONArray(0);
			// System.out.println(distance.toString());
			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.badRequest().build();
	}

	
	
	
	
	
	
	
	
	
	public static String readFileAsString(String file) throws Exception {
		return new String(Files.readAllBytes(Paths.get(file)));
	}

	@ApiOperation(value = "Creates passenger")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 422, message = "Name is already in use"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@RequestMapping(value = "/register-passenger", method = RequestMethod.POST)
	public ResponseEntity<?> savePassenger(@RequestBody UserDto user) throws Exception {
		user.setRole_string("PASSENGER");
		return ResponseEntity.ok(userDetailsService.save(user));
	}

	@ApiOperation(value = "Creates driver")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 422, message = "Name is already in use"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@RequestMapping(value = "/register-driver", method = RequestMethod.POST)
	public ResponseEntity<?> saveDriver(@RequestBody UserDto user) throws Exception {
		user.setRole_string("DRIVER");
		return ResponseEntity.ok(userDetailsService.save(user));
	}

//	private void firebaseauthenticate(String username, String password) throws Exception {
//		try {
//			authenticationManager.authenticate(new NamePasswordAuthenticationToken(username, password));
//		} catch (DisabledException e) {
//			throw new Exception("USER_DISABLED", e);
//		} catch (BadCredentialsException e) {
//			throw new Exception("INVALID_CREDENTIALS", e);
//		}
//	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	@ApiOperation(value = "Returns user from username", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity getUserByName(
			@ApiParam(value = "username", example = "user1") @RequestParam(name = "username") String username)
			throws Exception {
		UserDao user = userDetailsService.findByUsername(username);
		if (user == null)
			return ResponseEntity.badRequest().build();
		return ResponseEntity.ok(userDetailsService.findByUsername(username));
	}

	
	@RequestMapping(value = "/user/allpassengers", method = RequestMethod.GET)
	@ApiOperation(value = "Returns passenger", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity allPassenger()
			throws Exception {
		
		return ResponseEntity.ok(userDetailsService.findByRole_string("PASSENGER"));
	}
	
	@RequestMapping(value = "/user/alldrivers", method = RequestMethod.GET)
	@ApiOperation(value = "Returns ", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity allDrivers()
			throws Exception {
		
		return ResponseEntity.ok(userDetailsService.findByRole_string("DRIVER"));
	}

	
	
	
	
	
	@RequestMapping(value = "/find-by-token", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Returns user from token", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity getUserByToken(@RequestHeader("Authorization") String token) throws Exception {
		if (token != null && token.startsWith("Bearer ")) {
			String subtoken = token.substring(7);
			String username = jwtTokenUtil.getUsernameFromToken(subtoken);
			if (username == null)
				return ResponseEntity.badRequest().body("Non-exist user with the token");
			UserDao user = userDetailsService.findByUsername(username);
			if (user == null)
				return ResponseEntity.badRequest().body("Non-exist user with the token");

			return ResponseEntity.ok(user);

		}
		return ResponseEntity.badRequest().build();
	}

	@DeleteMapping("/user/delete-user/{id}")
	@ApiOperation(value = "Deletes specific user by userID")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 404, message = "The user doesn't exist"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity deleteUser(@ApiParam("id") @PathVariable long id) {
		UserDao user = userDetailsService.findByID(id);

		if (user == null) {
			return ResponseEntity.badRequest().build();
		}

		userDetailsService.deleteById(id);

		return ResponseEntity.ok().build();
	}

	@PutMapping("/user/update")
	public ResponseEntity userUpdate(@RequestBody UserDto user, @RequestParam(name = "password") String password)
			throws Exception {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}

		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		// System.out.println("Password chinh thuc: "+userDetails.getPassword());

		System.out.println("Password encoded: " + userDetails.getPassword());
		if (passwordEncoder.matches(password, userDetails.getPassword())) {
			if (user.getUsername() == null || user.getUsername().isEmpty()) {

				user.setUsername(username);
				System.out.println("IF 1");
				System.out.println("Name la: " + user.getUsername());
			}

			if (!user.getUsername().equals(username) && user.getUsername() != null) {
				System.out.println("Bad Request");
				return ResponseEntity.badRequest().build();
			}

			System.out.println("Name la: " + user.getUsername());
			System.out.println("Password la: " + user.getPassword());
			return ResponseEntity.ok(userDetailsService.update(user, username));
		}
		return ResponseEntity.badRequest().build();
	}
	
	@GetMapping(path = "/user/ingroup/{groupid}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all user in group by groupid", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAllByGroupID(@ApiParam("groupid") @PathVariable long groupid) {
		
		
		return new ResponseEntity<>(userDetailsService.findByGroupId(groupid), HttpStatus.OK);

	}
	
	@GetMapping(path = "/user/status", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all user by status", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAllByStatus(@ApiParam("status") @RequestParam(name = "status") String status) {
		System.out.println("IF 1");
		try {
			List<UserDao> listUser = userDetailsService.findByStatus(status);
			return new ResponseEntity<>(listUser, HttpStatus.OK);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			return ResponseEntity.badRequest().build();

		}
	}
	
	@GetMapping(path = "/user/available-seat/{groupid}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Available seat in group by groupid", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> availableSeat(@ApiParam("groupid") @PathVariable long groupid) {
		
		
		List<UserDao> listUser = userDetailsService.findByGroupId(groupid);
				
		Group group = groupService.findById(groupid);
		
		Vehicles vehicle = vehicleService.findByUserid(group.getUserid());
		
		int left = vehicle.getSeat() - listUser.size();
		return new ResponseEntity<>(left, HttpStatus.OK);


	}
	

	
//	@PutMapping("/admin/update-user")
//	public ResponseEntity adminUpdateUser(@RequestBody UserDao user, @RequestParam(name = "username") String username,
//			@RequestParam(name = "password") String password) throws Exception {
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String adminname = "";
//		if (principal instanceof UserDetails)
//			adminname = ((UserDetails) principal).getUsername();
//		else {
//			adminname = principal.toString();
//		}
//
//		final UserDetails userDetails = userDetailsService.loadUserByUsername(adminname);
//
//		if (passwordEncoder.matches(password, userDetails.getPassword())) {
//
//			UserDao newuser = userDetailsService.findByUsername(username);
//			if (newuser == null)
//				return ResponseEntity.badRequest().build();
//			if (user.getUsername() == null || user.getUsername().isEmpty()) {
//
//				user.setUsername(username);
//				System.out.println("IF 1");
//				System.out.println("Name la: " + user.getUsername());
//			}
//
//			if (!user.getUsername().equals(username) && user.getUsername() != null) {
//				System.out.println("Bad Request");
//				return ResponseEntity.badRequest().build();
//			}
//
//			System.out.println("Name la: " + user.getUsername());
//			System.out.println("Password la: " + user.getPassword());
//			return ResponseEntity.ok(userDetailsService.update(user, username));
//		}
//		return ResponseEntity.badRequest().build();
//	}

//	@PutMapping("/admin/update-user-config/{userid}")
//	public ResponseEntity adminUpdateUserConfig(@ApiParam("user") @RequestBody String userString, @PathVariable long userid) throws Exception {
//		UserDao user = userDetailsService.findByID(userid);
//		if(user ==null)
//		{
//			return ResponseEntity.badRequest().body("Invalid userid");
//		}
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String adminname = "";
//		System.out.println("den day");
//		if (principal instanceof UserDetails) {
//			adminname = ((UserDetails) principal).getUsername();
//			System.out.println("den day if");
//		}
//		else {
//			System.out.println("pincipal: "+principal.getClass().getSimpleName());
//			UserDao principalUserDao = (UserDao)principal;
//			adminname = principalUserDao.getUsername();
//			System.out.println(adminname);
//		}
//		long adminid = userDetailsService.findByUsername(adminname).getId();
//
//		JSONObject obj = new JSONObject(userString);
//		JSONObject config = obj.getJSONObject("config");
//		ArrayList<String> arl = new ArrayList<String>();
//		config.keys().forEachRemaining(key -> {
//	        String value = config.getString(key);
//	        System.out.println(key +" "+ value);
//	        arl.add(value);
//	    });
//		
//		UserDao userdao = new UserDao();
//		userdao.setName(obj.getString("username"));
//		userdao.setDisplayname(obj.getString("displayname"));
//		userdao.setEmail(obj.getString("email"));
//		userdao.setPhone(obj.getString("phone"));
//		userdao.setFacebook(obj.getString("facebook"));
//		userdao.setAddress(obj.getString("address"));
//		userdao.setLoginMethod(obj.getString("loginMethod"));
//		userdao.setPhotourl(obj.getString("photourl"));
//		userdao.setActive(obj.getBoolean("active"));
//		userdao.setRegisterDate(obj.getLong("registerDate"));
//		userdao.setLoginTime(obj.getLong("loginTime"));
//		userdao.setPhotourl(arl.get(0));
//		userdao.setYoutube_url(arl.get(1));
//		userdao.setAdmin_support(arl.get(2));
//		userdao.setHotline(arl.get(3));
//		
//		//userdao.setConfig(ex);
//		
//		userdao.setId(userid);
//		System.out.println(userdao);
//		return ResponseEntity.ok(userDetailsService.update(userdao, userdao.getName()));
//	}

	private void authenticate(String username, String password) throws Exception {
		try {

			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			e.printStackTrace();
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			e.printStackTrace();
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	@ApiOperation(value = "Returns all users", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 422, message = "Name is already in use"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@RequestMapping(value = "/user/allusers", method = RequestMethod.GET)
	public ResponseEntity<?> listAllUsers() throws Exception {

		return ResponseEntity.ok(userDetailsService.findAll(null, null, "id", Direction.ASC));
	}

//	UserDao EmailverifyIdToken(String idToken) throws InterruptedException, ExecutionException {
//		FirebaseToken decodedToken;
//		try {
//			decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
//			String uid = decodedToken.getUid();
//			System.out.println("uid: "+uid );
//			if (uid == null || uid.isEmpty()) {
//				return null;
//			}
//
//			UserDao userdao = userDetailsService.findByUid(uid);
//
//			UserRecord userRecord;
//			try {
//
//				userRecord = FirebaseAuth.getInstance().getUser(uid);
//
//				if (userdao == null) {
//					System.out.println("userdao null");
//					UserDto userdto = new UserDto();
//					userdto.setUid(userRecord.getUid());
//					userdto.setName(userRecord.getUid());
//					userdto.setEmail(userRecord.getEmail());
//					userdto.setPhone(userRecord.getPhoneNumber());
//					userdto.setPhotourl(userRecord.getPhotoUrl());
//					userdto.setRegisterDate(userRecord.getUserMetadata().getCreationTimestamp());
//					userdto.setLoginTime(userRecord.getUserMetadata().getLastSignInTimestamp());
//					userdto.setLoginMethod(userRecord.getProviderData()[0].getProviderId());
//					userdto.setDisplayname(userRecord.getDisplayName());
//					ListUsersPage page;
//					try {
//						page = FirebaseAuth.getInstance().listUsers(null);
//						for (ExportedUserRecord user : page.iterateAll()) {
//							System.out.println("User: " + user.getUid());
//							if(user.getUid().equals(userRecord.getUid()))
//							{
//								userdto.setPassword(user.getPasswordSalt());
//								break;
//							}
//						}
//					} catch (FirebaseAuthException e1) {
//						e1.printStackTrace();
//					}
//					
//					System.out.println(userdto.toString());
//					userDetailsService.saveFirbase(userdto);
//					return userDetailsService.findByUid(userRecord.getUid());
//					// return userDetailsService.loadUserByUid(userRecord.getUid());
//				} else {
//					System.out.println("userdetails not null");
//					// System.out.println(userDetailsService.loadUserByName(userRecord.getDisplayName()));
//					UserDao useerdao = userDetailsService.findByUid(userRecord.getUid());
//					System.out.println(useerdao.toString());
//					return useerdao;
//
//				}
//
//			} catch (FirebaseAuthException e) {
//				e.printStackTrace();
//			}
//
//			System.out.println("Decoded ID token from user: " + uid);
//
//		} catch (FirebaseAuthException e1) {
//			e1.printStackTrace();
//		}
//		return null;
//	}

//	@RequestMapping(value = "/firebase-login-email", method = RequestMethod.POST)
//	public ResponseEntity<?> firebaseLoginEmail(@ApiParam("idToken") @RequestBody String string) throws Exception {
//		JSONObject obj = new JSONObject(string);
//		String idToken = obj.getString("idToken");
//		
//		UserDao verifyIdToken = EmailverifyIdToken(idToken);
//		final String token = jwtTokenUtil.generateTokenUserDao(verifyIdToken);
//		
//		return ResponseEntity.ok(new JwtResponse(token));
//	} 

//	
//	@RequestMapping(value = "/get-app-config", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Returns youtube channel", response = List.class)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getYoutubeChannel() throws Exception {
//		JSONObject config = new JSONObject();
//		config.put("youtube_url", this.youtube_url);
//		config.put("hotline", this.hotline);
//		config.put("doc_url", this.doc_url);
//		config.put("admin_support", this.admin_support);
//		return new ResponseEntity<>(config.toMap(), HttpStatus.OK);
//	}

//	@RequestMapping(value = "/admin/set-app-config", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Set youtube channel", response = List.class)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"), 
//			@ApiResponse(code = 403, message = "Access denied"), 
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity setYoutubeChannel(@ApiParam("{\r\n" + 
//			"  \"youtube_url\": \"https://www.youtube.com/\",\r\n" +
//			"  \"hotline\": \"https://www.youtube.com/\",\r\n" +
//			"  \"doc_url\": \"https://www.youtube.com/\",\r\n" +
//			"  \"admin_support\": \"https://www.youtube.com/\",\r\n" +
//			"}") @RequestBody String jsonString) throws Exception {
//		JSONObject obj = new JSONObject(jsonString);
//		String youtube_url = obj.getString("youtube_url");
//		String hotline = obj.getString("hotline");
//		String doc_url = obj.getString("doc_url");
//		String admin_support = obj.getString("admin_support");
//		URL domain = new URL(youtube_url);
//		this.youtube_url = domain.toString();
//		this.hotline = hotline;
//		this.doc_url = doc_url;
//		this.admin_support = admin_support;
//		JSONObject config = new JSONObject();
//		config.put("youtube_url", this.youtube_url);
//		config.put("hotline", this.hotline);
//		config.put("doc_url", this.doc_url);
//		config.put("admin_support", this.admin_support);
//		return new ResponseEntity<>(config.toMap(), HttpStatus.OK);
//	}

//	@PostMapping("/user/uploadavatar")
//	public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile file) {
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String username = "";
//		if (principal instanceof UserDetails)
//			username = ((UserDetails) principal).getName();
//		else {
//			UserDao principalUserDao = (UserDao)principal;
//			username = principalUserDao.getName();
//		}
//		long userid = userDetailsService.findByName(username).getId();
//
//		UserDao user = userDetailsService.findByID(userid);
//		if (user == null) {
//			return ResponseEntity.badRequest().body("Invalid user");
//		}
//		String directoryName = "/home/mata/app/matadoorstatic/userimage/";
//		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		File directory = new File(directoryName);
//		if (!directory.exists()) {
//			directory.mkdir();
//		}
//		Path path = Paths
//				.get(directoryName + String.valueOf(user.getId() + "." + FilenameUtils.getExtension(fileName)));
//		try {
//			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
//			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/userimage/")
//					.path(String.valueOf(user.getId() + "." + FilenameUtils.getExtension(fileName))).toUriString();
//			System.out.println(ServletUriComponentsBuilder.fromCurrentContextPath());
//			user.setPhotourl(fileDownloadUri);
//			userDetailsService.userdaosaveFirbase(user);
//			return ResponseEntity.ok(fileDownloadUri);
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return ResponseEntity.ok().build();
//	}

//	@GetMapping(value = "/userimage/{userstring}", produces = MediaType.IMAGE_JPEG_VALUE)
//	public ResponseEntity<byte[]> getHeNhomImage(@PathVariable String userstring) throws IOException {
//		long userid = Long.valueOf(userstring.substring(0, userstring.lastIndexOf('.'))).longValue();
//		;
//		UserDao user = userDetailsService.findByID(userid);
//		if (user == null || user.getPhotourl() == null || user.getPhotourl().isEmpty()) {
//			return ResponseEntity.badRequest().build();
//		}
//
//		// File directory = new File(henhom.getPicture());
//		File directory = new File("/home/mata/app/matadoorstatic/userimage/" + userstring);
//
//		byte[] fileContent = Files.readAllBytes(directory.toPath());
//
//		return ResponseEntity.ok(fileContent);
//	}
}

//
//@RestController
//@CrossOrigin(origins = "*")
//public class UserController {
//	static String hotline = "";
//	static String youtube_url = "";
//	static String doc_url = "";
//	static String admin_support = "";
//	
//	
//	@Autowired
//	private PasswordEncoder passwordEncoder;
//
//	@Autowired
//	private AuthenticationManager authenticationManager;
//
//	@Autowired
//	private JwtTokenUtil jwtTokenUtil;
//
//	@Autowired
//	private JwtUserDetailsService userDetailsService;
//
//	
//	
//	UserDao EmailverifyIdToken(String idToken) throws InterruptedException, ExecutionException {
//		FirebaseToken decodedToken;
//		try {
//			decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
//			String uid = decodedToken.getUid();
//			System.out.println("uid: "+uid );
//			if (uid == null || uid.isEmpty()) {
//				return null;
//			}
//
//			UserDao userdao = userDetailsService.findByUid(uid);
//
//			UserRecord userRecord;
//			try {
//
//				userRecord = FirebaseAuth.getInstance().getUser(uid);
//
//				if (userdao == null) {
//					System.out.println("userdao null");
//					UserDto userdto = new UserDto();
//					userdto.setUid(userRecord.getUid());
//					userdto.setUsername(userRecord.getUid());
//					userdto.setEmail(userRecord.getEmail());
//					userdto.setPhone(userRecord.getPhoneNumber());
//					userdto.setPhotourl(userRecord.getPhotoUrl());
//					userdto.setRegisterDate(userRecord.getUserMetadata().getCreationTimestamp());
//					userdto.setLoginTime(userRecord.getUserMetadata().getLastSignInTimestamp());
//					userdto.setLoginMethod(userRecord.getProviderData()[0].getProviderId());
//					userdto.setDisplayname(userRecord.getDisplayName());
//					ListUsersPage page;
//					try {
//						page = FirebaseAuth.getInstance().listUsers(null);
//						for (ExportedUserRecord user : page.iterateAll()) {
//							System.out.println("User: " + user.getUid());
//							if(user.getUid().equals(userRecord.getUid()))
//							{
//								userdto.setPassword(user.getPasswordSalt());
//								break;
//							}
//						}
//					} catch (FirebaseAuthException e1) {
//						e1.printStackTrace();
//					}
//					
//					System.out.println(userdto.toString());
//					userDetailsService.saveFirbase(userdto);
//					return userDetailsService.findByUid(userRecord.getUid());
//					// return userDetailsService.loadUserByUid(userRecord.getUid());
//				} else {
//					System.out.println("userdetails not null");
//					// System.out.println(userDetailsService.loadUserByUsername(userRecord.getDisplayName()));
//					UserDao useerdao = userDetailsService.findByUid(userRecord.getUid());
//					System.out.println(useerdao.toString());
//					return useerdao;
//
//				}
//
//			} catch (FirebaseAuthException e) {
//				e.printStackTrace();
//			}
//
//			System.out.println("Decoded ID token from user: " + uid);
//
//		} catch (FirebaseAuthException e1) {
//			e1.printStackTrace();
//		}
//		return null;
//	}
//
//	@RequestMapping(value = "/firebase-login-email", method = RequestMethod.POST)
//	public ResponseEntity<?> firebaseLoginEmail(@ApiParam("idToken") @RequestBody String string) throws Exception {
//		JSONObject obj = new JSONObject(string);
//		String idToken = obj.getString("idToken");
//		
//		UserDao verifyIdToken = EmailverifyIdToken(idToken);
//		final String token = jwtTokenUtil.generateTokenUserDao(verifyIdToken);
//		
//		return ResponseEntity.ok(new JwtResponse(token));
//	} 
//
//	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
//	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
//		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
//		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
//		final String token = jwtTokenUtil.generateToken(userDetails);
//		return ResponseEntity.ok(new JwtResponse(token));
//	}
//
//	@ApiOperation(value = "Returns all users", produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 422, message = "Username is already in use"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	@RequestMapping(value = "/admin/allusers", method = RequestMethod.GET)
//	public ResponseEntity<?> listAllUsers() throws Exception {
//		JSONObject result = new JSONObject();
//		JSONArray fgh = new JSONArray();
//		
//		for( UserDao user : userDetailsService.findAll())
//		{
//			
//			JSONObject sampleInnerElement = new JSONObject();
//			sampleInnerElement.put("doc_url", user.getDoc_url());
//			sampleInnerElement.put("youtube_url", user.getYoutube_url());
//			sampleInnerElement.put("admin_support", user.getAdmin_support());
//			sampleInnerElement.put("hotline", user.getHotline());
//			
//			result.put("config", sampleInnerElement);
//			result.put("id", user.getId());
//			result.put("uid", user.getUid());
//			result.put("username", user.getUsername());
//			result.put("displayname", user.getDisplayname());
//			result.put("email", user.getEmail());
//			result.put("phone", user.getPhone());
//			result.put("facebook", user.getFacebook());
//			result.put("address", user.getAddress());
//			result.put("loginMethod", user.getLoginMethod());
//			result.put("photourl", user.getPhotourl());
//			result.put("active", user.getActive());
//			result.put("registerDate", user.getRegisterDate());
//			result.put("loginTime", user.getLoginTime());
//			
//			fgh.put(result);
//
//		}
//		return ResponseEntity.ok(fgh);
//	}
//	
//	@RequestMapping(value = "/get-app-config", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Returns youtube channel", response = List.class)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getYoutubeChannel() throws Exception {
//		JSONObject config = new JSONObject();
//		config.put("youtube_url", this.youtube_url);
//		config.put("hotline", this.hotline);
//		config.put("doc_url", this.doc_url);
//		config.put("admin_support", this.admin_support);
//		return new ResponseEntity<>(config.toMap(), HttpStatus.OK);
//	}
//	
//	@RequestMapping(value = "/admin/set-app-config", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Set youtube channel", response = List.class)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"), 
//			@ApiResponse(code = 403, message = "Access denied"), 
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity setYoutubeChannel(@ApiParam("{\r\n" + 
//			"  \"youtube_url\": \"https://www.youtube.com/\",\r\n" +
//			"  \"hotline\": \"https://www.youtube.com/\",\r\n" +
//			"  \"doc_url\": \"https://www.youtube.com/\",\r\n" +
//			"  \"admin_support\": \"https://www.youtube.com/\",\r\n" +
//			"}") @RequestBody String jsonString) throws Exception {
//		JSONObject obj = new JSONObject(jsonString);
//		String youtube_url = obj.getString("youtube_url");
//		String hotline = obj.getString("hotline");
//		String doc_url = obj.getString("doc_url");
//		String admin_support = obj.getString("admin_support");
//		URL domain = new URL(youtube_url);
//		this.youtube_url = domain.toString();
//		this.hotline = hotline;
//		this.doc_url = doc_url;
//		this.admin_support = admin_support;
//		JSONObject config = new JSONObject();
//		config.put("youtube_url", this.youtube_url);
//		config.put("hotline", this.hotline);
//		config.put("doc_url", this.doc_url);
//		config.put("admin_support", this.admin_support);
//		return new ResponseEntity<>(config.toMap(), HttpStatus.OK);
//	}
//	
//	
//	
//	@RequestMapping(value = "/admin/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Returns user from userID", response = List.class)
//	@ApiResponses(value = { 
//			@ApiResponse(code = 400, message = "Something went wrong"), 
//			@ApiResponse(code = 403, message = "Access denied"), 
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getUserById(@ApiParam("id") @PathVariable long id) throws Exception {
//		UserDao user = userDetailsService.findByID(id);
//		if (user == null)
//			return ResponseEntity.badRequest().build();
//		JSONObject result = new JSONObject();
//		JSONArray fgh = new JSONArray();
//		JSONObject sampleInnerElement = new JSONObject();
//		sampleInnerElement.put("doc_url", user.getDoc_url());
//		sampleInnerElement.put("youtube_url", user.getYoutube_url());
//		sampleInnerElement.put("admin_support", user.getAdmin_support());
//		sampleInnerElement.put("hotline", user.getHotline());
//		
//		result.put("config", sampleInnerElement);
//		result.put("id", user.getId());
//		result.put("uid", user.getUid());
//		result.put("username", user.getUsername());
//		result.put("displayname", user.getDisplayname());
//		result.put("email", user.getEmail());
//		result.put("phone", user.getPhone());
//		result.put("facebook", user.getFacebook());
//		result.put("address", user.getAddress());
//		result.put("loginMethod", user.getLoginMethod());
//		result.put("photourl", user.getPhotourl());
//		result.put("active", user.getActive());
//		result.put("registerDate", user.getRegisterDate());
//		result.put("loginTime", user.getLoginTime());
////		userdao.setUsername(obj.getString("username"));
////		userdao.setDisplayname(obj.getString("displayname"));
////		userdao.setEmail(obj.getString("email"));
////		userdao.setPhone(obj.getString("phone"));
////		userdao.setFacebook(obj.getString("facebook"));
////		userdao.setAddress(obj.getString("address"));
////		userdao.setLoginMethod(obj.getString("loginMethod"));
////		userdao.setPhotourl(obj.getString("photourl"));
////		userdao.setActive(obj.getBoolean("active"));
////		userdao.setRegisterDate(obj.getLong("registerDate"));
////		userdao.setLoginTime(obj.getLong("loginTime"));
//		return new ResponseEntity<>(result.toMap(), HttpStatus.OK);
//		//return ResponseEntity.ok(userDetailsService.findByID(id));
//		
//	}
//	
//	@ApiOperation(value = "Creates user")
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 422, message = "Username is already in use"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	@RequestMapping(value = "/register", method = RequestMethod.POST)
//	public ResponseEntity<?> saveUser(@RequestBody UserDto user) throws Exception {
//		return ResponseEntity.ok(userDetailsService.save(user));
//	}
//
//	private void firebaseauthenticate(String username, String password) throws Exception {
//		try {
//			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//		} catch (DisabledException e) {
//			throw new Exception("USER_DISABLED", e);
//		} catch (BadCredentialsException e) {
//			throw new Exception("INVALID_CREDENTIALS", e);
//		}
//	}
//
//	
//
//	@RequestMapping(value = "/admin/user", method = RequestMethod.GET)
//	@ApiOperation(value = "Returns user from username", response = List.class)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getUserByUsername(
//			@ApiParam(value = "username", example = "user1") @RequestParam(name = "username") String username)
//			throws Exception {
//		UserDao user = userDetailsService.findByUsername(username);
//		if (user == null)
//			return ResponseEntity.badRequest().build();
//		return ResponseEntity.ok(userDetailsService.findByUsername(username));
//	}
//
//	@RequestMapping(value = "/find-by-token", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Returns user from token", response = List.class)
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity getUserByToken(@RequestHeader("Authorization") String token) throws Exception {
//		if (token != null && token.startsWith("Bearer ")) {
//			String subtoken = token.substring(7);
//			String username = jwtTokenUtil.getUsernameFromToken(subtoken);
//			if (username == null)
//				return ResponseEntity.badRequest().build();
//			UserDao user = userDetailsService.findByUsername(username);
//			if (user == null)
//				return ResponseEntity.badRequest().build();
//			
//			JSONObject result = new JSONObject();
//			JSONObject sampleInnerElement = new JSONObject();
//			sampleInnerElement.put("doc_url", user.getDoc_url());
//			sampleInnerElement.put("youtube_url", user.getYoutube_url());
//			sampleInnerElement.put("admin_support", user.getAdmin_support());
//			sampleInnerElement.put("hotline", user.getHotline());
//			
//			result.put("config", sampleInnerElement);
//			result.put("id", user.getId());
//			result.put("uid", user.getUid());
//			result.put("username", user.getUsername());
//			result.put("displayname", user.getDisplayname());
//			result.put("email", user.getEmail());
//			result.put("phone", user.getPhone());
//			result.put("facebook", user.getFacebook());
//			result.put("address", user.getAddress());
//			result.put("loginMethod", user.getLoginMethod());
//			result.put("photourl", user.getPhotourl());
//			result.put("active", user.getActive());
//			result.put("registerDate", user.getRegisterDate());
//			result.put("loginTime", user.getLoginTime());
//			return new ResponseEntity<>(result.toMap(), HttpStatus.OK);
//			//return ResponseEntity.ok(userDetailsService.findByUsername(username));
//
//		}
//		return ResponseEntity.badRequest().build();
//	}
//
//	@DeleteMapping("/admin/delete-user/{id}")
//	@ApiOperation(value = "Deletes specific user by userID")
//	@ApiResponses(value = {
//			@ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 404, message = "The user doesn't exist"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity deleteUser(@ApiParam("id") @PathVariable long id) {
//		UserDao user = userDetailsService.findByID(id);
//
//		if (user == null) {
//			return ResponseEntity.badRequest().build();
//		}
//		for (RoleDao role : user.getRoles()) {
//			if (role.getRole().equals("ADMIN"))
//				return ResponseEntity.badRequest().build();
//		}
//		userDetailsService.deleteById(id);
//
//		return ResponseEntity.ok().build();
//	}
//
//	@PutMapping("/user/update")
//	public ResponseEntity userUpdate(@RequestBody UserDao user, @RequestParam(name = "password") String password)
//			throws Exception {
//
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String username = "";
//		if (principal instanceof UserDetails)
//			username = ((UserDetails) principal).getUsername();
//		else {
//			UserDao principalUserDao = (UserDao)principal;
//			username = principalUserDao.getUsername();
//		}
//
//		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
//		// System.out.println("Password chinh thuc: "+userDetails.getPassword());
//
//		System.out.println("Password encoded: " + userDetails.getPassword());
//		if (passwordEncoder.matches(password, userDetails.getPassword())) {
//			if (user.getUsername() == null || user.getUsername().isEmpty()) {
//
//				user.setUsername(username);
//				System.out.println("IF 1");
//				System.out.println("Username la: " + user.getUsername());
//			}
//
//			if (!user.getUsername().equals(username) && user.getUsername() != null) {
//				System.out.println("Bad Request");
//				return ResponseEntity.badRequest().build();
//			}
//
//			System.out.println("Username la: " + user.getUsername());
//			System.out.println("Password la: " + user.getPassword());
//			return ResponseEntity.ok(userDetailsService.update(user, username));
//		}
//		return ResponseEntity.badRequest().build();
//	}
//	
//	@PutMapping("/admin/update-user")
//	public ResponseEntity adminUpdateUser(@RequestBody UserDao user, @RequestParam(name = "username") String username,
//			@RequestParam(name = "password") String password) throws Exception {
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String adminname = "";
//		if (principal instanceof UserDetails)
//			adminname = ((UserDetails) principal).getUsername();
//		else {
//			adminname = principal.toString();
//		}
//
//		final UserDetails userDetails = userDetailsService.loadUserByUsername(adminname);
//
//		if (passwordEncoder.matches(password, userDetails.getPassword())) {
//
//			UserDao newuser = userDetailsService.findByUsername(username);
//			if (newuser == null)
//				return ResponseEntity.badRequest().build();
//			if (user.getUsername() == null || user.getUsername().isEmpty()) {
//
//				user.setUsername(username);
//				System.out.println("IF 1");
//				System.out.println("Username la: " + user.getUsername());
//			}
//
//			if (!user.getUsername().equals(username) && user.getUsername() != null) {
//				System.out.println("Bad Request");
//				return ResponseEntity.badRequest().build();
//			}
//
//			System.out.println("Username la: " + user.getUsername());
//			System.out.println("Password la: " + user.getPassword());
//			return ResponseEntity.ok(userDetailsService.update(user, username));
//		}
//		return ResponseEntity.badRequest().build();
//	}
//	
//	
//	@PutMapping("/admin/update-user-config/{userid}")
//	public ResponseEntity adminUpdateUserConfig(@ApiParam("user") @RequestBody String userString, @PathVariable long userid) throws Exception {
//		UserDao user = userDetailsService.findByID(userid);
//		if(user ==null)
//		{
//			return ResponseEntity.badRequest().body("Invalid userid");
//		}
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String adminname = "";
//		System.out.println("den day");
//		if (principal instanceof UserDetails) {
//			adminname = ((UserDetails) principal).getUsername();
//			System.out.println("den day if");
//		}
//		else {
//			System.out.println("pincipal: "+principal.getClass().getSimpleName());
//			UserDao principalUserDao = (UserDao)principal;
//			adminname = principalUserDao.getUsername();
//			System.out.println(adminname);
//		}
//		long adminid = userDetailsService.findByUsername(adminname).getId();
//
//		JSONObject obj = new JSONObject(userString);
//		JSONObject config = obj.getJSONObject("config");
//		ArrayList<String> arl = new ArrayList<String>();
//		config.keys().forEachRemaining(key -> {
//	        String value = config.getString(key);
//	        System.out.println(key +" "+ value);
//	        arl.add(value);
//	    });
//		
//		UserDao userdao = new UserDao();
//		userdao.setUsername(obj.getString("username"));
//		userdao.setDisplayname(obj.getString("displayname"));
//		userdao.setEmail(obj.getString("email"));
//		userdao.setPhone(obj.getString("phone"));
//		userdao.setFacebook(obj.getString("facebook"));
//		userdao.setAddress(obj.getString("address"));
//		userdao.setLoginMethod(obj.getString("loginMethod"));
//		userdao.setPhotourl(obj.getString("photourl"));
//		userdao.setActive(obj.getBoolean("active"));
//		userdao.setRegisterDate(obj.getLong("registerDate"));
//		userdao.setLoginTime(obj.getLong("loginTime"));
//		
////		ExtraUserInfor ex = new ExtraUserInfor();
////		ex.setDoc_url(arl.get(0));
////		ex.setYoutube_url(arl.get(1));
////		ex.setAdmin_support(arl.get(2));
////		ex.setHotline(arl.get(3));
//		
//		userdao.setPhotourl(arl.get(0));
//		userdao.setYoutube_url(arl.get(1));
//		userdao.setAdmin_support(arl.get(2));
//		userdao.setHotline(arl.get(3));
//		
//		//userdao.setConfig(ex);
//		
//		userdao.setId(userid);
//		System.out.println(userdao);
//		return ResponseEntity.ok(userDetailsService.update(userdao, userdao.getUsername()));
//	}
//	
//
//	private void authenticate(String username, String password) throws Exception {
//		try {
//
//			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//		} catch (DisabledException e) {
//			e.printStackTrace();
//			throw new Exception("USER_DISABLED", e);
//		} catch (BadCredentialsException e) {
//			e.printStackTrace();
//			throw new Exception("INVALID_CREDENTIALS", e);
//		}
//	}
//
//	@PostMapping("/user/uploadavatar")
//	public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile file) {
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String username = "";
//		if (principal instanceof UserDetails)
//			username = ((UserDetails) principal).getUsername();
//		else {
//			UserDao principalUserDao = (UserDao)principal;
//			username = principalUserDao.getUsername();
//		}
//		long userid = userDetailsService.findByUsername(username).getId();
//
//		UserDao user = userDetailsService.findByID(userid);
//		if (user == null) {
//			return ResponseEntity.badRequest().body("Invalid user");
//		}
//		String directoryName = "/home/mata/app/matadoorstatic/userimage/";
//		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		File directory = new File(directoryName);
//		if (!directory.exists()) {
//			directory.mkdir();
//		}
//		Path path = Paths
//				.get(directoryName + String.valueOf(user.getId() + "." + FilenameUtils.getExtension(fileName)));
//		try {
//			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
//			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/userimage/")
//					.path(String.valueOf(user.getId() + "." + FilenameUtils.getExtension(fileName))).toUriString();
//			System.out.println(ServletUriComponentsBuilder.fromCurrentContextPath());
//			user.setPhotourl(fileDownloadUri);
//			userDetailsService.userdaosaveFirbase(user);
//			return ResponseEntity.ok(fileDownloadUri);
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return ResponseEntity.ok().build();
//	}
//
//	@GetMapping(value = "/userimage/{userstring}", produces = MediaType.IMAGE_JPEG_VALUE)
//	public ResponseEntity<byte[]> getHeNhomImage(@PathVariable String userstring) throws IOException {
//		long userid = Long.valueOf(userstring.substring(0, userstring.lastIndexOf('.'))).longValue();
//		;
//		UserDao user = userDetailsService.findByID(userid);
//		if (user == null || user.getPhotourl() == null || user.getPhotourl().isEmpty()) {
//			return ResponseEntity.badRequest().build();
//		}
//
//		// File directory = new File(henhom.getPicture());
//		File directory = new File("/home/mata/app/matadoorstatic/userimage/" + userstring);
//
//		byte[] fileContent = Files.readAllBytes(directory.toPath());
//
//		return ResponseEntity.ok(fileContent);
//	}
//}
