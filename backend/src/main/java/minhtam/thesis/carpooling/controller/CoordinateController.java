package minhtam.thesis.carpooling.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import minhtam.thesis.carpooling.model.Coordinate;
import minhtam.thesis.carpooling.model.Group;
import minhtam.thesis.carpooling.model.UserDao;
import minhtam.thesis.carpooling.model.Vehicles;
import minhtam.thesis.carpooling.service.CoordinateService;
import minhtam.thesis.carpooling.service.JwtUserDetailsService;

@RestController
@CrossOrigin(origins = "*")
public class CoordinateController {
	@Autowired
	private ModelMapper modelMapper;


	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private CoordinateService coordinateService;
	
	@RequestMapping(value = "/user/create-coordinate", method = RequestMethod.POST)
	@ApiOperation(value = "Create new coordinate")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 422, message = "Cong trinh is already exist"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> createCoordinatesss(@ApiParam("new coordinate") @RequestBody Coordinate newcoordinate)
			throws Exception {
		System.out.println("begin");
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		if(newcoordinate.getType() == null || newcoordinate.getType().isEmpty())
		{
			return ResponseEntity.badRequest().body("Empty coordinate type");

		}
		
		List<Coordinate> listCoor = coordinateService.findByUserid(userid);
		
		for(Coordinate coor : listCoor)
		{
			if(coor.getType() == "origin" && newcoordinate.getType() == "origin")
			{
				return ResponseEntity.badRequest().body("Error create new coordinate: The user already has an origin");
			}
			if(coor.getType() == "destination" && newcoordinate.getType() == "destination")
			{
				return ResponseEntity.badRequest().body("Error create new coordinate: The user already has an destination");
			}
		}
		newcoordinate.setUserid(userid);
		return new ResponseEntity<>(coordinateService.save(modelMapper.map(newcoordinate, Coordinate.class)), HttpStatus.CREATED);

	}
	
	@GetMapping(path = "/user/{userid}/coordinate", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all coordinate of user by userid", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAllByUserID(@ApiParam("userid") @PathVariable long userid) {
		
		return new ResponseEntity<>(coordinateService.findByUserid(userid), HttpStatus.OK);
	}
	
	@PutMapping("/user/update-coordinate/{coordinateid}")
	public ResponseEntity<?> UpdateVehicless(@ApiParam("coordinateid") @PathVariable long coordinateid,
			@ApiParam("coordinate") @RequestBody Coordinate coordinate) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		System.out.println(userid);

		Coordinate newCoordinate = coordinateService.findById(coordinateid);
		if (newCoordinate == null) {
			return ResponseEntity.badRequest().body("Cannot find coordinate from coordinateid");
		}
		if (newCoordinate.getUserid() == userid) {
			coordinate.setId(newCoordinate.getId());
			return ResponseEntity.ok(coordinateService.save(coordinate));
		}
		return ResponseEntity.badRequest().build();

	}
}
