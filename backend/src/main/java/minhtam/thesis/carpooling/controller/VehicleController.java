package minhtam.thesis.carpooling.controller;



import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;


import io.swagger.annotations.*;
import minhtam.thesis.carpooling.model.Group;
import minhtam.thesis.carpooling.model.UserDao;
import minhtam.thesis.carpooling.model.Vehicles;
import minhtam.thesis.carpooling.service.JwtUserDetailsService;
import minhtam.thesis.carpooling.service.VehicleService;

@RestController
@CrossOrigin(origins = "*")
public class VehicleController {

	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private ModelMapper modelMapper;


	@Autowired
	private JwtUserDetailsService userDetailsService;
	@GetMapping(path = "/user/vehicle/{id}")
	@ApiOperation(value = " Return vehicle by ID", response = Vehicles.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findById(@PathVariable long id) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		if (vehicleService.findById(id).getUserid() == userid) {
			return ResponseEntity.ok(vehicleService.findById(id));
		}
		return ResponseEntity.badRequest().build();

	}

	


//	@RequestMapping(value = "/driver/create-vehicle", method = RequestMethod.POST)
//	@ApiOperation(value = "Create new vehicle")
//	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 422, message = "Cong trinh is already exist"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public ResponseEntity<?> createVehicless(@ApiParam("new vehicle") @RequestBody Vehicles newvehicle)
//			 {
//		System.out.println("begin");
//		try {
//		
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		String username = "";
//		if (principal instanceof UserDetails)
//			username = ((UserDetails) principal).getUsername();
//		else {
//			UserDao principalUserDao = (UserDao)principal;
//			username = principalUserDao.getUsername();
//		}
//		long userid = userDetailsService.findByUsername(username).getId();
//		
//		if(vehicleService.findByUserid(userid) != null) {
//			return ResponseEntity.badRequest().body("Users just can have 1 instance of vehicle");
//		}
//		newvehicle.setUserid(userid);
//		//return ResponseEntity.ok(vehicleService.save(modelMapper.map(newcongtrinh, Vehicless.class)));
//		vehicleService.save(newvehicle);
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//			return ResponseEntity.badRequest().build(); 
//		}
//		return ResponseEntity.ok(newvehicle);
//
//	}
	
	@RequestMapping(value = "/driver/create-vehicle", method = RequestMethod.POST)
	@ApiOperation(value = "Create new vehicle")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 422, message = "Cong trinh is already exist"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> createGroups(@ApiParam("new group") @RequestBody Vehicles newvehicle)
			throws Exception {
		System.out.println("begin");
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		
		long userid = userDetailsService.findByUsername(username).getId();
		if(vehicleService.findByUserid(userid) != null) {
			return ResponseEntity.badRequest().body("Users just can have 1 instance of vehicle");
		}
		
		newvehicle.setUserid(userid);
		//return ResponseEntity.ok(groupService.save(modelMapper.map(newcongtrinh, Groups.class)));
		return new ResponseEntity<>(vehicleService.save(modelMapper.map(newvehicle, Vehicles.class)), HttpStatus.CREATED);

	}
	
	
	

	@PutMapping("/user/update-vehicle/{vehicleid}")
	public ResponseEntity<?> UpdateVehicless(@ApiParam("vehicleid") @PathVariable long vehicleid,
			@ApiParam("congtrinh") @RequestBody Vehicles congtrinh) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		System.out.println(userid);

		Vehicles newVehicless = vehicleService.findById(vehicleid);
		if (newVehicless == null) {
			return ResponseEntity.badRequest().body("Cannot find vehicle from vehicleid");
		}
		if (newVehicless.getUserid() == userid) {
			congtrinh.setId(newVehicless.getId());
			return ResponseEntity.ok(vehicleService.save(congtrinh));
		}
		return ResponseEntity.badRequest().build();

	}


	@DeleteMapping("/user/delete-vehicle/{id}")
	@ApiOperation(value = "Deletes vehicle he nhom by vehicleID")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> DeleteVehicless(@ApiParam("id") @PathVariable long id) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		Vehicles newVehicless = vehicleService.findById(id);
		if (newVehicless == null)
			return ResponseEntity.badRequest().build();
		if (newVehicless.getUserid() == userid) {
			vehicleService.deleteById(id);

			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}

	@GetMapping(path = "/user/allvehicles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all group", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAll() {
		
		return new ResponseEntity<>(vehicleService.findAll(null, null, "id", Direction.ASC), HttpStatus.OK);
	}
	
	@GetMapping(path = "/user/{userid}/vehicle", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all vehicle of user by userid", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAllByUserID(@ApiParam("userid") @PathVariable long userid) {
		
		return new ResponseEntity<>(vehicleService.findByUserid(userid), HttpStatus.OK);
	}
	
	
}