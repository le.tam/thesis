package minhtam.thesis.carpooling.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;
import minhtam.thesis.carpooling.model.Group;
import minhtam.thesis.carpooling.model.UserDao;
import minhtam.thesis.carpooling.model.UserDto;
import minhtam.thesis.carpooling.model.Vehicles;
import minhtam.thesis.carpooling.service.GroupService;
import minhtam.thesis.carpooling.service.JwtUserDetailsService;
import minhtam.thesis.carpooling.service.VehicleService;

@RestController
@CrossOrigin(origins = "*")
public class GroupController {
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private GroupService groupService;
	
	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	
	
	@GetMapping(path = "/user/group/{id}")
	@ApiOperation(value = " Return group by ID", response = Group.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findById(@PathVariable long id) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		
		long userid = userDetailsService.findByUsername(username).getId();
		//if (groupService.findById(id).getUserid() == userid) {
			return ResponseEntity.ok(groupService.findById(id));
		//}
		//return ResponseEntity.badRequest().build();

	}

	


	@RequestMapping(value = "/driver/create-group", method = RequestMethod.POST)
	@ApiOperation(value = "Create new group")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 422, message = "Cong trinh is already exist"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> createGroups(@ApiParam("new group") @RequestBody Group newgroup)
			throws Exception {
		System.out.println("begin");
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		
		long userid = userDetailsService.findByUsername(username).getId();
		Group group = groupService.findByUserid(userid);
		if(group !=null)
		{
			return ResponseEntity.badRequest().body("This driver already has another group");

		}
		newgroup.setUserid(userid);
		//return ResponseEntity.ok(groupService.save(modelMapper.map(newcongtrinh, Groups.class)));
		return new ResponseEntity<>(groupService.save(modelMapper.map(newgroup, Group.class)), HttpStatus.CREATED);

	}

	@PutMapping("/driver/update-group/{groupid}")
	public ResponseEntity<?> UpdateGroups(@ApiParam("groupid") @PathVariable long groupid,
			@ApiParam("congtrinh") @RequestBody Group congtrinh) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		System.out.println(userid);

		Group newGroups = groupService.findById(groupid);
		if (newGroups == null) {
			return ResponseEntity.badRequest().body("Cannot find group from groupid");
		}
		if (newGroups.getUserid() == userid) {
			congtrinh.setId(newGroups.getId());
			return ResponseEntity.ok(groupService.save(congtrinh));
		}
		return ResponseEntity.badRequest().build();

	}


	@DeleteMapping("/driver/delete-group/{id}")
	@ApiOperation(value = "Deletes group  by groupID")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> DeleteGroups(@ApiParam("id") @PathVariable long id) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		Group newGroups = groupService.findById(id);
		if (newGroups == null)
			return ResponseEntity.badRequest().build();
		try {
		if (newGroups.getUserid() == userid) {
			List<UserDao> listUser = userDetailsService.findByGroupId(id);
			for(UserDao userdao : listUser)
			{
				UserDto userdto = new UserDto();
				userdto.setDate_of_join(userdao.getDate_of_join());
				userdto.setE_time_start(userdao.getE_time_start());
				userdto.setEmail(userdao.getEmail());
				userdto.setGender(userdao.getGender());
				userdto.setGroupid(null);
				userdto.setL_time_start(userdao.getL_time_start());
				userdto.setLast_login_date(userdao.getLast_login_date());
				userdto.setPhone(userdao.getPhone());
				userdto.setRole_string(userdao.getRole_string());
				userdto.setTotal_trip(userdao.getTotal_trip());
				userdto.setUsername(userdao.getUsername());
				userdto.setStatus(userdao.getStatus());
				userDetailsService.save(userdto);

			}

			groupService.deleteById(id);
			vehicleService.deleteByUserid(userid);
			userDetailsService.deleteById(userid);

			

			return ResponseEntity.ok().build();
		}
		}
		catch(Exception e)
		{
			return ResponseEntity.badRequest().build();

		}
		return ResponseEntity.badRequest().build();
	}

	@GetMapping(path = "/user/allgroups", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all group", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAll() {
		
		return new ResponseEntity<>(groupService.findAll(null, null, "id", Direction.ASC), HttpStatus.OK);
	}
	
	@GetMapping(path = "/user/{userid}/group", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = " Return all group of user by userid", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> findAllByUserID(@ApiParam("userid") @PathVariable long userid) {
		try {
		Group group = groupService.findByUserid(userid);
		return new ResponseEntity<>(group, HttpStatus.OK);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ResponseEntity.badRequest().build();

	}
	
	@GetMapping(path = "/user/leave-group/{groupid}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Leave group", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> leaveGroup(@ApiParam("groupid") @PathVariable long groupid) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		
		List<UserDao> listUser = userDetailsService.findByGroupId(groupid);
		
		Group group = groupService.findById(groupid);
		
		Vehicles vehicle = vehicleService.findByUserid(group.getUserid());
		
		boolean inGroup = false;
		long userdaoid = -1;
		for(UserDao userdao : listUser)
		{
			if(userdao.getId() == userid)
			{
				inGroup = true;
				userdaoid = userdao.getId();
				break;
			}
		}
		if(inGroup == false)
		{
			return ResponseEntity.badRequest().body("This user is not in this group");

		}
		
		
		try {
			UserDao userdao = userDetailsService.findByID(userdaoid);
			if(userdao.getRole_string() == "DRIVER")
			{
				return ResponseEntity.badRequest().body("ERROR: This user is driver");

			}
			userdao.setGroupid(null);
			UserDto userdto = new UserDto();
			userdto.setDate_of_join(userdao.getDate_of_join());
			userdto.setE_time_start(userdao.getE_time_start());
			userdto.setEmail(userdao.getEmail());
			userdto.setGender(userdao.getGender());
			userdto.setGroupid(null);
			userdto.setL_time_start(userdao.getL_time_start());
			userdto.setLast_login_date(userdao.getLast_login_date());
			userdto.setPhone(userdao.getPhone());
			userdto.setRole_string(userdao.getRole_string());
			userdto.setTotal_trip(userdao.getTotal_trip());
			userdto.setUsername(userdao.getUsername());
			userDetailsService.update(userdto,userdao.getUsername());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.badRequest().body("ERROR: Unsuccesfully saving user");

		}

	
		listUser = userDetailsService.findByGroupId(groupid);
		int left = vehicle.getSeat() - listUser.size();
		if(left == 0)
		{
			group.setStatus("full");
		}
		else if(left == vehicle.getSeat())
		{
			group.setStatus("inactive");
		}
		else if(left >= 1 && left<vehicle.getSeat())
		{
			group.setStatus("active");

		}
		return ResponseEntity.ok().body("This user left group successfully");
		
		
		
		//return new ResponseEntity<>(groupService.findByUserid(userid), HttpStatus.OK);
	}
	
	@GetMapping(path = "/user/join-group/{groupid}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Join group", response = Group.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
			@ApiResponse(code = 403, message = "Access denied"),
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	public ResponseEntity<?> joinGroup(@ApiParam("groupid") @PathVariable long groupid) {
		System.out.println("begin");

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails)
			username = ((UserDetails) principal).getUsername();
		else {
			UserDao principalUserDao = (UserDao)principal;
			username = principalUserDao.getUsername();
		}
		long userid = userDetailsService.findByUsername(username).getId();
		System.out.println("begin");
		try {

		UserDao userdao = userDetailsService.findByID(userid);
		Group group = groupService.findById(groupid);
		Vehicles vehicle = vehicleService.findByUserid(group.getUserid());
		if(vehicle==null)
		{
			return ResponseEntity.badRequest().body("Non-exist vehicle");

		}
		List<UserDao> listUser = userDetailsService.findByGroupId(groupid);

		if(userdao.getGroupid()!=null)
		{
			return ResponseEntity.badRequest().body("This user is in another group");

		}
		if(group.getUserid() == userid)
		{
			return ResponseEntity.badRequest().body("This user is already in this group");

		}
		int left = vehicle.getSeat() - listUser.size();

		if(left <=0)
		{
			return ResponseEntity.badRequest().body("Group is full");

		}
		
		
		try {
			userdao.setGroupid(groupid);
			UserDto userdto = new UserDto();
			userdto.setDate_of_join(userdao.getDate_of_join());
			userdto.setE_time_start(userdao.getE_time_start());
			userdto.setEmail(userdao.getEmail());
			userdto.setGender(userdao.getGender());
			userdto.setGroupid(groupid);
			userdto.setL_time_start(userdao.getL_time_start());
			userdto.setLast_login_date(userdao.getLast_login_date());
			userdto.setPhone(userdao.getPhone());
			userdto.setRole_string(userdao.getRole_string());
			userdto.setTotal_trip(userdao.getTotal_trip());
			userdto.setUsername(userdao.getUsername());
			userDetailsService.update(userdto,userdao.getUsername());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.badRequest().body("ERROR: Unsuccesfully saving user");

		}
		left = left -1;
		if(left == 0)
		{
			group.setStatus("full");
		}
		else if(left == vehicle.getSeat())
		{
			group.setStatus("inactive");
		}
		else if(left >= 1 && left<vehicle.getSeat())
		{
			group.setStatus("active");

		}
		return ResponseEntity.ok().body("This user join group successfully");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ResponseEntity.badRequest().build();

	}
	
}