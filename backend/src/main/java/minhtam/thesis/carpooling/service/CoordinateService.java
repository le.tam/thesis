package minhtam.thesis.carpooling.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import minhtam.thesis.carpooling.model.Coordinate;
import minhtam.thesis.carpooling.repository.CoordinateRepository;


@Service
public class CoordinateService {
	@Autowired
	private CoordinateRepository coordinateRepository;

	public List<Coordinate> findAll(Integer pageNo, Integer pageSize, String sortBy,Sort.Direction direction) {
		Pageable paging = Pageable.unpaged();
		if (pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
			System.out.println("Den day");
		}
		
		if (direction.equals(Direction.DESC) && pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
			System.out.println("Den day");
		}
		Page<Coordinate> congTrinh = coordinateRepository.findAll(paging);
		if (congTrinh.hasContent()) {
			return congTrinh.getContent();
		} else {
			return new ArrayList<Coordinate>();
		}
	}
	
	public List<Coordinate> findByUserid(long userid) {
		List<Coordinate> group = coordinateRepository.findByUserid(userid);
		return group;
	}

	public Coordinate save(Coordinate group) {

		return coordinateRepository.save(group);
	}

	public Coordinate findById(long id) {
		Coordinate group = coordinateRepository.findById(id).get();
		return group;
	}
	
	public Coordinate findByUseridAndType(long userid, String type) {
		Coordinate group = coordinateRepository.findByUseridAndType(userid, type);
		return group;
	}
	
	public void deleteById(long id) {
		coordinateRepository.deleteById(id);
	}
}
