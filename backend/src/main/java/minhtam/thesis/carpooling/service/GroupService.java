package minhtam.thesis.carpooling.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import minhtam.thesis.carpooling.model.Group;
import minhtam.thesis.carpooling.model.UserDao;
import minhtam.thesis.carpooling.repository.GroupRepository;



@Service
public class GroupService {

	@Autowired
	private GroupRepository groupRepository;

	public List<Group> findAll(Integer pageNo, Integer pageSize, String sortBy,Sort.Direction direction) {
		Pageable paging = Pageable.unpaged();
		if (pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
			System.out.println("Den day");
		}
		
		if (direction.equals(Direction.DESC) && pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
			System.out.println("Den day");
		}
		Page<Group> congTrinh = groupRepository.findAll(paging);
		if (congTrinh.hasContent()) {
			return congTrinh.getContent();
		} else {
			return new ArrayList<Group>();
		}
	}
	
	
	public Group findByUserid(long userid) {
		Group group = groupRepository.findByUserid(userid);
		return group;
	}

	public Group save(Group group) {

		return groupRepository.save(group);
	}

	public Group findById(long id) {
		Group group = groupRepository.findById(id).get();
		return group;
	}

	public void deleteById(long id) {
		groupRepository.deleteById(id);
	}

}