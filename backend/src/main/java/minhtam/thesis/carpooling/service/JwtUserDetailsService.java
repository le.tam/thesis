package minhtam.thesis.carpooling.service;

import minhtam.thesis.carpooling.exception.CustomException;
import minhtam.thesis.carpooling.model.RoleDao;
import minhtam.thesis.carpooling.model.UserDao;
import minhtam.thesis.carpooling.model.UserDto;
import minhtam.thesis.carpooling.repository.RoleRepository;
import minhtam.thesis.carpooling.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userDao;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@PersistenceContext
	private EntityManager manager;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDao user = userDao.findByUsername(username);
		if (user == null) {
			System.out.println("user null");
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		try {
			System.out.println("den day");
			List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
			org.springframework.security.core.userdetails.User newuser = new org.springframework.security.core.userdetails.User(
					user.getUsername(), user.getPassword(), authorities);
			System.out.println(newuser);
			return newuser;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}


	private List<GrantedAuthority> getUserAuthority(Set<RoleDao> userRoles) {
		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		for (RoleDao role : userRoles) {
			roles.add(new SimpleGrantedAuthority(role.getRole()));
		}
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
		return grantedAuthorities;
	}

	public UserDao save(UserDto user) {
		if (userDao.findByUsername(user.getUsername()) == null) {
			try {
			System.out.println("1");
			UserDao newUser = new UserDao();
			newUser.setUsername(user.getUsername());
			newUser.setEmail(user.getEmail());
			newUser.setDisplayname(user.getDisplayname());
			System.out.println("4");
			//newUser.setCoordinate(user.getCoordinate());
			//newUser.setDistance(user.getDistance());
			newUser.setTotal_trip(user.getTotal_trip());
			newUser.setDate_of_join(user.getDate_of_join());
			newUser.setLast_login_date(user.getLast_login_date());
			newUser.setRole_string(user.getRole_string());
			newUser.setE_time_start(user.getE_time_start());
			newUser.setL_time_start(user.getL_time_start());
			newUser.setGroupid(user.getGroupid());
			newUser.setPhone(user.getPhone());
			newUser.setGender(user.getGender());
			newUser.setStatus(user.getStatus());
			
			System.out.println("3");
			newUser.setPassword(bcryptEncoder.encode(user.getPassword()));

			System.out.println("2");
			
			RoleDao userRole = roleRepository.findByRole(user.getRole_string());
			newUser.setRoles(new HashSet<RoleDao>(Arrays.asList(userRole)));
			return userDao.save(newUser);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new CustomException("Error creating new user", HttpStatus.UNPROCESSABLE_ENTITY);
			}
		} else {
			throw new CustomException("Name is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

//	public UserDao saveFirbase(UserDto user) {
//		if (userDao.findByName(user.getName()) == null) {
//			System.out.println("1");
//			UserDao newUser = new UserDao();
//			newUser.setUid(user.getUid());
//			newUser.setName(user.getName());
//			newUser.setEmail(user.getEmail());
//			newUser.setPhone(user.getPhone());
//			newUser.setAddress(user.getAddress());
//			newUser.setRegisterDate(user.getRegisterDate());
//			newUser.setLoginTime(user.getLoginTime());
//			newUser.setPhotourl(user.getPhotourl());
//			newUser.setPassword(user.getPassword());
//			newUser.setFacebook(user.getFacebook());
//			newUser.setLoginMethod(user.getLoginMethod());
//			newUser.setDisplayname((user.getDisplayname()));
//			System.out.println("2");
//			// newUser.setRoles(Arrays.asList("USER"));
//			newUser.setActive(true);
//			RoleDao userRole = roleRepository.findByRole("USER");
//			newUser.setRoles(new HashSet<RoleDao>(Arrays.asList(userRole)));
//			return userDao.save(newUser);
//		} else {
//			throw new CustomException("Name is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
//		}
//	}

	public UserDao userdaosaveFirbase(UserDao user) {

		return userDao.save(user);

	}

	public List<UserDao> findAll(Integer pageNo, Integer pageSize, String sortBy,Sort.Direction direction) {
		Pageable paging = Pageable.unpaged();
		System.out.println(pageNo);
		if (pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
			System.out.println("Den day");
		}
		
		if (direction.equals(Direction.DESC) && pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
			System.out.println("Den day");
		}
		return userDao.findAll(paging);
	}
	
	public List<UserDao> findByGroupId(long groupid) {
		return userDao.findByGroupid(groupid);
	}
	
	public List<UserDao> findByStatus(String status) {
		return userDao.findByStatus(status);
	}
	
	public List<UserDao> findByRole_string(String role_string) {
		return userDao.findByRole_String(role_string);
	}

	private UserDetails buildUserForAuthentication(UserDao user, List<GrantedAuthority> authorities) {
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				true, true, true, true, authorities);
	}

	public UserDao findByID(long id) {
		return userDao.findByID(id);

	}

	public UserDao findByUsername(String username) {
		return userDao.findByUsername(username);

	}

	@Transactional
	public void deleteById(long id) {
		try {
			Query query = manager.createNativeQuery("DELETE FROM user_role WHERE user_id = " + id);
			query.executeUpdate();
			query = manager.createNativeQuery("DELETE FROM users WHERE id = " + id);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public UserDao update(UserDto user, String username) throws Exception {
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		UserDao pre_userdao = userDao.findByUsername(username);
		if (pre_userdao != null) {
			System.out.println("Den day chua");
			UserDao newUser = userDao.findByUsername(user.getUsername());

			if (user.getUsername() != null && !user.getUsername().isEmpty())
				newUser.setUsername(user.getUsername());
			if (user.getDisplayname() != null && !user.getDisplayname().isEmpty())
				newUser.setDisplayname(user.getDisplayname());
			
			if (user.getEmail() != null && !user.getEmail().isEmpty())
				newUser.setEmail(user.getEmail());
			
			if (user.getPassword() != null && !user.getPassword().isEmpty()) {
				System.out.println(user.getPassword());
				newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
			}
			
			if (user.getPhone() != null && !user.getPhone().isEmpty())
				newUser.setPhone(user.getPhone());
			
			if (user.getStatus() != null && !user.getStatus().isEmpty())
				newUser.setStatus(user.getStatus() );
			
			if (user.getGender() != null && !user.getGender().isEmpty())
				newUser.setGender(user.getGender());
			
//			if (user.getCoordinate()!= null)
//				newUser.setCoordinate(user.getCoordinate());
//			if (user.getDistance() != null)
//				newUser.setDistance(user.getDistance());
			if (user.getTotal_trip() != null)
				newUser.setTotal_trip(user.getTotal_trip());
			
			if (user.getDate_of_join() != null)
				newUser.setDate_of_join(user.getDate_of_join());
			if (user.getLast_login_date() != null)
				newUser.setLast_login_date(user.getLast_login_date());
			if (user.getE_time_start() != null)
				newUser.setE_time_start(user.getE_time_start());
			if (user.getL_time_start() != null)
				newUser.setL_time_start(user.getL_time_start());
			
			if (user.getGroupid() != null)
				newUser.setGroupid(user.getGroupid());
			
			
			return userDao.save(newUser);
		} else {
			throw new CustomException("Cannot find user", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

}
//
//@Service
//public class JwtUserDetailsService implements UserDetailsService {
//	@Autowired
//	private JwtUserDetailsService userDetailsService;
//
//	@Autowired
//	private AuthenticationManager authenticationManager;
//
//	@Autowired
//	private UserRepository userDao;
//
//	@Autowired
//	private RoleRepository roleRepository;
//
//	@Autowired
//	private PasswordEncoder bcryptEncoder;
//
//	@PersistenceContext
//	private EntityManager manager;
//
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		UserDao user = userDao.findByUsername(username);
//		if (user == null) {
//			System.out.println("user null");
//			throw new UsernameNotFoundException("User not found with username: " + username);
//		}
//		try {
//			System.out.println("den day");
//			List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
//			org.springframework.security.core.userdetails.User newuser = new org.springframework.security.core.userdetails.User(
//					user.getUsername(), user.getPassword(), authorities);
//			System.out.println(newuser);
//			return newuser;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//
//	}
//
//
//	private List<GrantedAuthority> getUserAuthority(Set<RoleDao> userRoles) {
//		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
//		for (RoleDao role : userRoles) {
//			roles.add(new SimpleGrantedAuthority(role.getRole()));
//		}
//		List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
//		return grantedAuthorities;
//	}
//
//	public UserDao save(UserDto user) {
//		if (userDao.findByUsername(user.getUsername()) == null) {
//			System.out.println("1");
//			UserDao newUser = new UserDao();
//			newUser.setUid(user.getUid());
//			newUser.setUsername(user.getUsername());
//			newUser.setEmail(user.getEmail());
//			newUser.setPhone(user.getPhone());
//			newUser.setAddress(user.getAddress());
//			newUser.setRegisterDate(user.getRegisterDate());
//			newUser.setLoginMethod(user.getLoginMethod());
//			newUser.setLoginTime(user.getLoginTime());
//			newUser.setPhotourl(user.getPhotourl());
//			newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
//			newUser.setDisplayname((user.getDisplayname()));
////			newUser.setDoc_url(user.getDoc_url());
////			newUser.setYoutube_url(user.getYoutube_url());;
////			newUser.setAdmin_support(user.getAdmin_support());
////			newUser.setHotline(user.getHotline());
//			System.out.println("2");
//			// newUser.setRoles(Arrays.asList("USER"));
//			newUser.setActive(true);
//			RoleDao userRole = roleRepository.findByRole("USER");
//			newUser.setRoles(new HashSet<RoleDao>(Arrays.asList(userRole)));
//			return userDao.save(newUser);
//		} else {
//			throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
//		}
//	}
//
//	public UserDao saveFirbase(UserDto user) {
//		if (userDao.findByUsername(user.getUsername()) == null) {
//			System.out.println("1");
//			UserDao newUser = new UserDao();
//			newUser.setUid(user.getUid());
//			newUser.setUsername(user.getUsername());
//			newUser.setEmail(user.getEmail());
//			newUser.setPhone(user.getPhone());
//			newUser.setAddress(user.getAddress());
//			newUser.setRegisterDate(user.getRegisterDate());
//			newUser.setLoginTime(user.getLoginTime());
//			newUser.setPhotourl(user.getPhotourl());
//			newUser.setPassword(user.getPassword());
//			newUser.setFacebook(user.getFacebook());
//			newUser.setLoginMethod(user.getLoginMethod());
//			newUser.setDisplayname((user.getDisplayname()));
////			newUser.setDoc_url(user.getDoc_url());
////			newUser.setYoutube_url(user.getYoutube_url());;
////			newUser.setAdmin_support(user.getAdmin_support());
////			newUser.setHotline(user.getHotline());
//			System.out.println("2");
//			// newUser.setRoles(Arrays.asList("USER"));
//			newUser.setActive(true);
//			RoleDao userRole = roleRepository.findByRole("USER");
//			newUser.setRoles(new HashSet<RoleDao>(Arrays.asList(userRole)));
//			return userDao.save(newUser);
//		} else {
//			throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
//		}
//	}
//
//	public UserDao userdaosaveFirbase(UserDao user) {
//
//		return userDao.save(user);
//
//	}
//
//	public List<UserDao> findAll() {
//		return userDao.findAll();
//	}
//
//	private UserDetails buildUserForAuthentication(UserDao user, List<GrantedAuthority> authorities) {
//		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
//				user.getActive(), true, true, true, authorities);
//	}
//
//	public UserDao findByID(long id) {
//		return userDao.findByID(id);
//
//	}
//
//	public UserDao findByUsername(String username) {
//		return userDao.findByUsername(username);
//
//	}
//
//	@Transactional
//	public void deleteById(long id) {
//		try {
//			Query query = manager.createNativeQuery("DELETE FROM user_role WHERE user_id = " + id);
//			query.executeUpdate();
//			query = manager.createNativeQuery("DELETE FROM users WHERE id = " + id);
//			query.executeUpdate();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public UserDao findByUid(String uid) {
//		return userDao.findByUid(uid);
//	}
//
//	public UserDao update(UserDao user, String username) throws Exception {
//		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
//		if (userDao.findByUsername(username) != null) {
//			System.out.println("Den day chua");
//			UserDao newUser = userDao.findByUsername(user.getUsername());
//
//			if (user.getUsername() != null && !user.getUsername().isEmpty())
//				newUser.setUsername(user.getUsername());
//			if (user.getEmail() != null && !user.getEmail().isEmpty())
//				newUser.setEmail(user.getEmail());
//			if (user.getPhone() != null && !user.getPhone().isEmpty())
//				newUser.setPhone(user.getPhone());
//			if (user.getAddress() != null && !user.getAddress().isEmpty())
//				newUser.setAddress(user.getAddress());
//			if (user.getDisplayname()!= null && !user.getDisplayname().isEmpty())
//				newUser.setDisplayname(user.getDisplayname());
//			if (user.getPhotourl() != null && !user.getPhotourl().isEmpty())
//				newUser.setPhotourl(user.getPhotourl());
//			if (user.getLoginMethod() != null && !user.getLoginMethod().isEmpty())
//				newUser.setLoginMethod(user.getLoginMethod());
//			if (user.getFacebook() != null && !user.getFacebook().isEmpty())
//				newUser.setFacebook(user.getFacebook());
//			
////			if (user.getConfig() != null)
////				newUser.setConfig(user.getConfig());
//			
//			if (user.getDoc_url() != null && !user.getDoc_url().isEmpty())
//				newUser.setDoc_url(user.getDoc_url());
//			if (user.getYoutube_url() != null && !user.getYoutube_url().isEmpty())
//				newUser.setYoutube_url(user.getYoutube_url());
//			if (user.getAdmin_support() != null && !user.getAdmin_support().isEmpty())
//				newUser.setAdmin_support(user.getAdmin_support());
//			if (user.getHotline() != null && !user.getHotline().isEmpty())
//				newUser.setHotline(user.getHotline());
//			
//			if (user.getUid() != null && !user.getUid().isEmpty())
//				newUser.setUid(user.getUid());
//			if (user.getRegisterDate() != 0L)
//				newUser.setRegisterDate(user.getRegisterDate());
//			if (user.getLoginTime() != 0L)
//				newUser.setLoginTime(user.getLoginTime());
//			if (user.getPassword() != null && !user.getPassword().isEmpty()) {
//				System.out.println(user.getPassword());
//				newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
//			}
//			// System.out.println(user.getPassword());
//			if (user.getActive() != null)
//				newUser.setActive(user.getActive());
//			// authenticate(newUser.getUsername(), newUser.getPassword());
//			return userDao.save(newUser);
//		} else {
//			throw new CustomException("Cannot find user", HttpStatus.UNPROCESSABLE_ENTITY);
//		}
//	}
//
//}