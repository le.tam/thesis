package minhtam.thesis.carpooling.service;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import minhtam.thesis.carpooling.model.Vehicles;
import minhtam.thesis.carpooling.repository.VehicleRepository;




@Service
public class VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;

	public List<Vehicles> findAll(Integer pageNo, Integer pageSize, String sortBy,Sort.Direction direction) {
		Pageable paging = Pageable.unpaged();
		if (pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
			System.out.println("Den day");
		}
		
		if (direction.equals(Direction.DESC) && pageNo != null) {
			paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
			System.out.println("Den day");
		}
		Page<Vehicles> congTrinh = vehicleRepository.findAll(paging);
		if (congTrinh.hasContent()) {
			return congTrinh.getContent();
		} else {
			return new ArrayList<Vehicles>();
		}
	}
	
	public Vehicles findByUserid(long userid) {
		Vehicles group = vehicleRepository.findByUserid(userid);
		return group;
	}

	public Vehicles save(Vehicles group) {

		return vehicleRepository.save(group);
	}

	public Vehicles findById(long id) {
		Vehicles group = vehicleRepository.findById(id).get();
		return group;
	}

	public void deleteById(long id) {
		vehicleRepository.deleteById(id);
	}
	
	public void deleteByUserid(long id) {
		vehicleRepository.deleteByUserid(id);
	}

}