package minhtam.thesis.carpooling.model;


import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicles")
public class Vehicles {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	
	@Column(name = "userid",nullable = false)
	private long userid;
	

	@Column(name = "vehicle_type")
	private String vehicle_type;
	
	@Column(name = "vehicle_manufacturer")
	private String vehicle_manufacturer;
	
	@Column(name = "seat")
	private int seat;
	
	@Column(name = "driver_license")
	private String driver_license;
	
	
	
	@Column(name = "license_plate")
	private String license_plate;
	
	@Column(name = "license_start_date")
	private String license_start_date;
	
	@Column(name = "license_expire_date")
	private String license_expire_date;

	
	@Transient
	private String description;
	
}