//package minhtam.thesis.carpooling.model;
//
//
//import org.hibernate.validator.constraints.Length;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//import javax.persistence.*;
//import javax.validation.constraints.Email;
//import javax.validation.constraints.NotEmpty;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Builder
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@Entity
//@Table(name = "destinations")
//public class Destinations {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "destination_id")
//	private long destination_id;
//
//	
//	@Column(name = "member",nullable = false)
//	private long member;
//	
//
//	@Column(name = "name")
//	private String name;
//	
//	@Column(name = "longitude")
//	private String longitude;
//	
//	@Column(name = "latitude")
//	private String latitude;
//	
//	@Column(name = "detour")
//	private long detour;
//
//	@Transient
//	private String description;
//	
//}