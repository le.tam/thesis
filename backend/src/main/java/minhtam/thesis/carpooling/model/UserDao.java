package minhtam.thesis.carpooling.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.*;

//
//@Entity
//@Table(name = "users")
//public class UserDao {
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private long id;
//	
//	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
//	@JoinTable(name = "user_role", joinColumns = {
//			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
//					@JoinColumn(name = "role_id", referencedColumnName = "id") })
//	private Set<RoleDao> roles;
//	
//
//	@Column(name = "role_string",nullable = false)
//	private String role_string;
//	
//	@Column(name = "group",nullable = false)
//	private long groupid;

//@Column(name = "vehicleid",nullable = false)
//private long vehicleid;
//
//@Column(name = "coordinate",nullable = false)
//private long coordinate;
//
//
//@Column
//private long distance;
//
//@Column
//private int total_trip;
//	
//@Column
//@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
//private String date_of_join;
//
//@Column
//@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
//private String last_login_date;


//	
//	@Column
//	private String email;
//	
//	@Column
//	@JsonProperty(access = Access.WRITE_ONLY)
//	private String password;
//	
//	@Column
//	private String username;
//	
//	
//	
//	

//
//
//	@Column(name = "uid", unique = true)
//	@JsonProperty(access = Access.WRITE_ONLY)
//	private String uid;
//	
//	
//
//
//	// @JsonIgnore
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public Set<RoleDao> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<RoleDao> roles) {
//		this.roles = roles;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	
//
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	
//
//	public long getGroupid() {
//		return groupid;
//	}
//
//	public void setGroupid(long groupid) {
//		this.groupid = groupid;
//	}
//
//	public long getVehicleid() {
//		return vehicleid;
//	}
//
//	public void setVehicleid(long vehicleid) {
//		this.vehicleid = vehicleid;
//	}
//
//	public long getCoordinate() {
//		return coordinate;
//	}
//
//	public void setCoordinate(long coordinate) {
//		this.coordinate = coordinate;
//	}
//
//	public long getDistance() {
//		return distance;
//	}
//
//	public void setDistance(long distance) {
//		this.distance = distance;
//	}
//
//	public int getTotal_trip() {
//		return total_trip;
//	}
//
//	public void setTotal_trip(int total_trip) {
//		this.total_trip = total_trip;
//	}
//
//	
//
//	public String getUid() {
//		return uid;
//	}
//
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
//
//	public String getRole_string() {
//		return role_string;
//	}
//
//	public void setRole_string(String role_string) {
//		this.role_string = role_string;
//	}
//
//	public String getDate_of_join() {
//		return date_of_join;
//	}
//
//	public void setDate_of_join(String date_of_join) {
//		this.date_of_join = date_of_join;
//	}
//
//	public String getLast_login_date() {
//		return last_login_date;
//	}
//
//	public void setLast_login_date(String last_login_date) {
//		this.last_login_date = last_login_date;
//	}
//	
//	
//	
//	
//	
//	
//}


@Entity
@Table(name = "users")
public class UserDao {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String username;

	@Column
	private String displayname;
	
	@Column(name = "e_time_startt")
	private String e_time_start;
	
	@Column(name = "l_time_startt")
	private String  l_time_start;

	@Column
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	@Column
	private String email;
	
	@Column
	private String phone;
	
	@Column
	private String gender;

	
	
//	@Transient
//	private List<CayTheoMaNhom> listCayTheoMaNhom = new ArrayList<CayTheoMaNhom>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id") })
	private Set<RoleDao> roles;

	@Column(name = "role_string",nullable = false)
	private String role_string;
	
	@Column(name = "groupid")
	private Long groupid;
	
	@Column(name = "status")
	private String status = "available";
	
//	@Column(name = "vehicleid",nullable = false)
//	private long vehicleid;
	
//	@Column(name = "coordinate",nullable = false)
//	private long coordinate;
	
	
//	@Column
//	private long distance;
	
	@Column
	private Integer total_trip;
		
	@Column
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private String date_of_join;
	
	@Column
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private String last_login_date;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	// @JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Set<RoleDao> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleDao> roles) {
		this.roles = roles;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

//	public List<CayTheoMaNhom> getListCayTheoMaNhom() {
//		return listCayTheoMaNhom;
//	}
//
//	public void setListCayTheoMaNhom(List<CayTheoMaNhom> listCayTheoMaNhom) {
//		this.listCayTheoMaNhom = listCayTheoMaNhom;
//	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	

	public String getRole_string() {
		return role_string;
	}

	public void setRole_string(String role_string) {
		this.role_string = role_string;
	}

//	public long getGroupid() {
//		return groupid;
//	}
//
//	public void setGroupid(long groupid) {
//		this.groupid = groupid;
//	}
//
//	public long getVehicleid() {
//		return vehicleid;
//	}
//
//	public void setVehicleid(long vehicleid) {
//		this.vehicleid = vehicleid;
//	}

//	public long getCoordinate() {
//		return coordinate;
//	}
//
//	public void setCoordinate(long coordinate) {
//		this.coordinate = coordinate;
//	}

//	public long getDistance() {
//		return distance;
//	}
//
//	public void setDistance(long distance) {
//		this.distance = distance;
//	}

	public Integer getTotal_trip() {
		return total_trip;
	}

	public void setTotal_trip(Integer total_trip) {
		this.total_trip = total_trip;
	}

	public String getDate_of_join() {
		return date_of_join;
	}

	public void setDate_of_join(String date_of_join) {
		this.date_of_join = date_of_join;
	}

	public String getLast_login_date() {
		return last_login_date;
	}

	public void setLast_login_date(String last_login_date) {
		this.last_login_date = last_login_date;
	}

	public String getE_time_start() {
		return e_time_start;
	}

	public void setE_time_start(String e_time_start) {
		this.e_time_start = e_time_start;
	}

	public String getL_time_start() {
		return l_time_start;
	}

	public void setL_time_start(String l_time_start) {
		this.l_time_start = l_time_start;
	}

	public Long getGroupid() {
		return groupid;
	}

	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
}
