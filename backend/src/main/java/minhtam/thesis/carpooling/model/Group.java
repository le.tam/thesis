package minhtam.thesis.carpooling.model;


import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "grouptable")
public class Group {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "userid",nullable = false)
	private long userid;
	
	@Column(name = "status")
	@Builder.Default
	private String status = "inactive";
	
	@Column(name = "period_of_existence")
	private long period_of_existence;
	
	@Column(name = "date_of_founded")
	private String date_of_founded;
	
	@Column(name = "last_trip")
	private String last_trip;
	
	@Column(name = "total_trips")
	private int total_trips;

	
	@Transient
	private String description;
	
}