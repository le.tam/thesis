//
//
//package minhtam.thesis.carpooling.model;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.annotation.JsonProperty.Access;
//
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "drivers")
//public class Drivers {
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private long driver_id;
//	
//	@Column(name = "group",nullable = false)
//	private long group;
//	
//	@Column(name = "vehicle",nullable = false)
//	private long vehicle;
//	
//	@Column(name = "origin",nullable = false)
//	private long origin;
//	
//	@Column(name = "destination",nullable = false)
//	private long destination;
//	
//	@Column
//	private String email;
//	
//	@Column
//	@JsonProperty(access = Access.WRITE_ONLY)
//	private String password;
//
//
//	@Column
//	private String name;
//
//	
//	@Column
//	private long distance;
//	
//	@Column
//	private int total_trip;
//		
//	@Column
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	private LocalDateTime date_of_join;
//	
//	@Column
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	private LocalDateTime last_login_date;
//
//	
//	
//
//
//	@Column(name = "uid", unique = true)
//	@JsonProperty(access = Access.WRITE_ONLY)
//	private String uid;
//
//
//	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
//	@JoinTable(name = "user_role", joinColumns = {
//			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
//					@JoinColumn(name = "role_id", referencedColumnName = "id") })
//	private Set<RoleDao> roles;
//
//	
//
//	// @JsonIgnore
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	
//	public Set<RoleDao> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<RoleDao> roles) {
//		this.roles = roles;
//	}
//
//
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	
//
//	
//	public String getUid() {
//		return uid;
//	}
//
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
//
//	
//
//	public long getDriver_id() {
//		return driver_id;
//	}
//
//	public void setDriver_id(long driver_id) {
//		this.driver_id = driver_id;
//	}
//
//
//	public long getDistance() {
//		return distance;
//	}
//
//	public void setDistance(long distance) {
//		this.distance = distance;
//	}
//
//
//	public int getTotal_trip() {
//		return total_trip;
//	}
//
//	public void setTotal_trip(int total_trip) {
//		this.total_trip = total_trip;
//	}
//
//	public LocalDateTime getDate_of_join() {
//		return date_of_join;
//	}
//
//	public void setDate_of_join(LocalDateTime date_of_join) {
//		this.date_of_join = date_of_join;
//	}
//
//	public LocalDateTime getLast_login_date() {
//		return last_login_date;
//	}
//
//	public void setLast_login_date(LocalDateTime last_login_date) {
//		this.last_login_date = last_login_date;
//	}
//
//	public long getGroup() {
//		return group;
//	}
//
//	public void setGroup(long group) {
//		this.group = group;
//	}
//
//	public long getVehicle() {
//		return vehicle;
//	}
//
//	public void setVehicle(long vehicle) {
//		this.vehicle = vehicle;
//	}
//
//	public long getOrigin() {
//		return origin;
//	}
//
//	public void setOrigin(long origin) {
//		this.origin = origin;
//	}
//
//	public long getDestination() {
//		return destination;
//	}
//
//	public void setDestination(long destination) {
//		this.destination = destination;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//	
//	
//	
//	
//
//}
