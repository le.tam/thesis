package minhtam.thesis.carpooling.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;


import io.swagger.annotations.ApiModelProperty;

public class UserDto {
	private Long groupid;
	private String email;
	private String password;
	private String role_string;
	
	
	private String username;
	//private Long vehicleid;
	//private Long coordinate;
	//private Long distance;
	private Integer total_trip;
	private String date_of_join;
	private String phone;
	private String gender;
	private String last_login_date;
	private String status;

	private String e_time_start;
	private String  l_time_start;
	private String displayname;

	
	
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
//	public Long getCoordinate() {
//		return coordinate;
//	}
//	public void setCoordinate(Long coordinate) {
//		this.coordinate = coordinate;
//	}
//	public Long getDistance() {
//		return distance;
//	}
//	public void setDistance(Long distance) {
//		this.distance = distance;
//	}
//	
	
	public Integer getTotal_trip() {
		return total_trip;
	}
	public void setTotal_trip(Integer total_trip) {
		this.total_trip = total_trip;
	}
	public String getDate_of_join() {
		return date_of_join;
	}
	public void setDate_of_join(String date_of_join) {
		this.date_of_join = date_of_join;
	}
	public String getLast_login_date() {
		return last_login_date;
	}
	public void setLast_login_date(String last_login_date) {
		this.last_login_date = last_login_date;
	}
	
	public String getRole_string() {
		return role_string;
	}
	public void setRole_string(String role_string) {
		this.role_string = role_string;
	}
	public String getE_time_start() {
		return e_time_start;
	}
	public void setE_time_start(String e_time_start) {
		this.e_time_start = e_time_start;
	}
	public String getL_time_start() {
		return l_time_start;
	}
	public void setL_time_start(String l_time_start) {
		this.l_time_start = l_time_start;
	}
	public Long getGroupid() {
		return groupid;
	}
	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
	
//	private String phone;
//	private String address; 
//	private Long registerDate;
//	private Long loginTime;
//	private String photourl;
//	private String uid;
//	private String facebook;
//	private String loginMethod;
//	private String displayname;
//	private String doc_url;
//	private String youtube_url;
//	private String admin_support;
//	private String hotline;
//
//	
//	
	
	
	
}

//
//public class UserDto {
//	@ApiModelProperty(value = "username", example = "test")
//	// @ApiModelProperty(position = 0)
//	private String username;
//	@ApiModelProperty(value = "password", example = "test")
//	// @ApiModelProperty(position = 1)
//	private String password;
//	@ApiModelProperty(value = "email", example = "test")
//	// @ApiModelProperty(position = 2)
//	private String email;
//	@ApiModelProperty(value = "phone", example = "0123456789")
//	// @ApiModelProperty(position = 3)
//	private String phone;
//	@ApiModelProperty(value = "address", example = "27 HoÃ ng Hoa ThÃ¡m, P12, BÃ¬nh Tháº¡nh")
//	// @ApiModelProperty(position = 4)
//	private String address;
//	@ApiModelProperty(value = "registerDate", example = "20/2/2020")
//	// @ApiModelProperty(position = 5)
//	private long registerDate;
//	@ApiModelProperty(value = "loginTime", example = "20/2/2020")
//	// @ApiModelProperty(position = 6)
//	private long loginTime;
//	private String photourl;
//	private String uid;
//	private String facebook;
//	private String loginMethod;
//	private String displayname;
//	private String doc_url;
//	private String youtube_url;
//	private String admin_support;
//	private String hotline;
//	//private ExtraUserInfor config;
//	
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public long getRegisterDate() {
//		return registerDate;
//	}
//
//	public void setRegisterDate(long registerDate) {
//		this.registerDate = registerDate;
//	}
//
//	public long getLoginTime() {
//		return loginTime;
//	}
//
//	public void setLoginTime(long loginTime) {
//		this.loginTime = loginTime;
//	}
//
//	public String getPhotourl() {
//		return photourl;
//	}
//
//	public void setPhotourl(String photourl) {
//		this.photourl = photourl;
//	}
//
//	public String getUid() {
//		return uid;
//	}
//
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
//
//	public String getFacebook() {
//		return facebook;
//	}
//
//	public void setFacebook(String facebook) {
//		this.facebook = facebook;
//	}
//
//	public String getLoginMethod() {
//		return loginMethod;
//	}
//
//	public void setLoginMethod(String loginMethod) {
//		this.loginMethod = loginMethod;
//	}
//
//	@Override
//	public String toString() {
//		return "UserDto [username=" + username + ", password=" + password + ", email=" + email + ", phone=" + phone
//				+ ", address=" + address + ", registerDate=" + registerDate + ", loginTime=" + loginTime + ", photourl="
//				+ photourl + ", uid=" + uid + ", facebook=" + facebook + ", loginMethod=" + loginMethod + "]";
//	}
//
//	public String getDisplayname() {
//		return displayname;
//	}
//
//	public void setDisplayname(String displayname) {
//		this.displayname = displayname;
//	}
//
//	public String getDoc_url() {
//		return doc_url;
//	}
//
//	public void setDoc_url(String doc_url) {
//		this.doc_url = doc_url;
//	}
//
//	public String getYoutube_url() {
//		return youtube_url;
//	}
//
//	public void setYoutube_url(String youtube_url) {
//		this.youtube_url = youtube_url;
//	}
//
//	public String getAdmin_support() {
//		return admin_support;
//	}
//
//	public void setAdmin_support(String admin_support) {
//		this.admin_support = admin_support;
//	}
//
//	public String getHotline() {
//		return hotline;
//	}
//
//	public void setHotline(String hotline) {
//		this.hotline = hotline;
//	}
//	
//
//	
//}
